# Controls Nodes

* [Button](nodes/CONTROLBUTTON.md)
* [Toggle](nodes/CONTROLTOGGLE.md)
* [Slider](nodes/SLIDER.md)
* [Indicator](nodes/INDICATOR.md)
* [Label](nodes/LABEL.md)
* [Line Graph](nodes/LINE_GRAPH.md)
* [Number Input](nodes/NUMBER_INPUT.md)
* [Text Input](nodes/TEXT_INPUT.md)