# A Throbbing Red Heart

1\. Connect [LED Panel] block

<small>Note: refer to <a href="../hardware/output/led-panel.html" target="_blank">[LED Panel]</a> for block introduction.</small>

2\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts area.

<img src="images/throbbing-heart-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. Add 4 Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED panel (1) shows image () for (1) secs</span> to make a throbbing heart.

<img src="images/throbbing-heart-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. Click the green flag to run the program

<img src="images/throbbing-heart-3.png" style="padding:3px 0px 15px 0px;">

4\. You can create new image, like a star, or a snowflake.

<img src="images/throbbing-heart-4.png" style="padding:3px 0px 15px 0px;">

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>Tips:</strong><br>
&#9755; Right-click the block/blocks to duplicate the script<br>
<img src="images/throbbing-heart-5.png" style="padding:3px 0px 15px 10px;width:300px;"><br>
&#9755; Right-click in the blank area of the Scripts Area
<img src="images/throbbing-heart-6.png" style="padding:3px 0px 15px 10px;width:300px;">
</div>

5\. Save your project.