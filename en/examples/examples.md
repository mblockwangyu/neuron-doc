# Examples

### Beginners

* [A Throbbing Red Heart](a-throbbing-red-heart.md)
* [Play Music with the Buzzer](play-music-with-buzzer.md)
* [Musical Heartbeat - Parallel Programming](musical-heartbeat.md)

### Intermediate

* [Face Changing Show](face-changing-show.md)
* [A Panda Counting Off](panda-counting-off.md)
* [Play Music with Funny Touch](play-music-with-funny-touch.md)

<a href="examples.rar" download>Click to download code</a>

---

### More Resources

<img src="images/education.png" style="padding: 0px 3px 3px 0px;width:150px;">

Explore more resources at [makeblock education]: <br>
[http://education.makeblock.com/resource/](http://education.makeblock.com/resource/)


