# Face Changing Show

This project will use the Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">gyro sensor (1) shaken?</span>

<img src="images/face-changing-4.png" style="padding:3px 0px 15px 0px;">

We'll also use the Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span> to change the image displayed on the LED Panel. When the condition is True, the block/blocks inside will run; otherwise, the block/blocks will be skipped.

<img src="images/face-changing-5.png" style="padding:3px 0px 15px 0px;">

We need these two Neuron blocks: <a href="../hardware/input/gyro.html" target="_blank">[Gyro]</a>, and <a href="../hardware/output/led-panel.html" target="_blank">[LED Panel]</a>.

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span>, a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">gyro sensor (1) shaken?</span>, and a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED panel (1) shows image () for (1) secs</span>. Change the image as follows:

<img src="images/face-changing-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. Duplicate the script and change the image accordingly.

<img src="images/face-changing-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/face-changing-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. Click the green flag to run the program. Shake the Gyro block and check the LED Panel.