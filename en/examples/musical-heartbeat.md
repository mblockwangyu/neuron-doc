# Musical Heartbeat - Parallel Programming

In mBlock 5, we line up the coding blocks to write a script. A maximum of 6 scripts can run at the same time.

This project consists of 2 scripts: one for the throbbing heart, the other for the music.

We need these two Neuron blocks: <a href="./hardware/output/buzzer.html" target="_blank">[Buzzer]</a>, and <a href="./hardware/output/led-panel.html" target="_blank">[LED Panel]</a>.

### Script one: play music with the buzzer

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area. Add four Sound blocks <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">buzzer (1) plays note () for () beats</span>, and choose notes: C4, D4, E4, and C4.

<img src="images/music-buzzer-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. Add more Sound blocks <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">buzzer (1) plays note () for () beats</span> as follows. Remember to use the "Duplicate" function.

<img src="images/music-buzzer-3.png" style="padding:3px 0px 15px 0px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/musical-heartbeat-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

### Script two: the throbbing heart

4\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts area, and then add two Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED panel (1) shows image () for (1) secs</span>.

<img src="images/musical-heartbeat-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

5\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/musical-heartbeat-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

6\. Click the green flag to run the program.

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>Tips:</strong><br>
&#9755; Use the Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span> to run the script repeatedly.
</div>
