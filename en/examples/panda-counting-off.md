# A Panda Counting Off

This project will use the Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">gyro sensor (1) shaken?</span>

<img src="images/face-changing-4.png" style="padding:3px 0px 15px 0px;">

When the gyro sensor detects any vibration ( it can sense motions like shaking or tapping the table), the output value will be true. If the gyro sensor fails to detect any vibration, the output value will be false.

In this project, we will use variables to have the Panda report the shaking strength detected by the Gyro block.

We need the <a href="./hardware/input/gyro.html" target="_blank">[Gyro]</a> block.

### Program the gyro

1\. Select Variables block, and click "Make a variable". Name the variable "Shaking Strength".

<img src="images/panda-counting-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area, and then add a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Set (Shaking Strength) to (0)</span>

<img src="images/panda-counting-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span>, a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">gyro sensor (1) shaken?</span>, and a Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">change (Shaking Strength) by (1)</span>

<img src="images/panda-counting-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/panda-counting-4.gif" style="padding:3px 0px 15px 0px; width:800px;">

### Program the panda

5\. Select "Panda" under "Sprites"

<small>Note: you can select another sprite, too.</small>

<img src="images/panda-counting-5.png" style="padding:3px 0px 15px 0px;">

6\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span>, a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">say ()</span>, and put the Variables block <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">Shaking Strength</span> inside the Looks block.

<img src="images/panda-counting-6.gif" style="padding:3px 0px 15px 0px; width:800px;">

7\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/panda-counting-7.gif" style="padding:3px 0px 15px 0px; width:800px;">

8\. Click the green flag to run the program. Then tap the table with your hands. See what happens?

<img src="images/panda-counting-8.png" style="padding:3px 0px 15px 0px;width：400px;">

This program can make a fun game. Compete with your friends to see who can get the best score within a given period of time.