# Play Music with the Buzzer

This project uses the Neuron Buzzer Block to play sounds and create a piece of music.

1\. Connect [Buzzer] block

<small>Note: refer to <a href="../hardware/output/buzzer.html" target="_blank">[Buzzer]</a> for block introduction.</small>

2\. We'll use the Sound block <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">Buzzer (1) plays note () for () beats</span>

<img src="images/music-buzzer-1.png" style="padding:3px 0px 15px 0px;">

3\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area. Add four Sound blocks <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">buzzer (1) plays note () for () beats</span>, and choose notes: C4, D4, E4, and C4.

<img src="images/music-buzzer-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. Add more Sound blocks <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">buzzer (1) plays note () for () beats</span> as follows. Remember to use the "Duplicate" function.

<img src="images/music-buzzer-3.png" style="padding:3px 0px 15px 0px;">

5\. Click the green flag to run the program. Listen! Which song is it?

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>Tips:</strong><br>
&#9755; You can set different numbers of beats to control the speed of the music.
</div>