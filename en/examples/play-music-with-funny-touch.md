# Play Music with Funny Touch

We can use message broadcast to have Neuron communicate with sprites, thus to make a interactive project.

<img src="images/funny-touch-1.png" style="padding:3px 0px 15px 0px;">

We need the <a href="./hardware/input/touch.html" target="_blank">[Funny Touch]</a> block.

### Program Funny Touch

1\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">if () then ()</span>, a Sensing block <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">Funny Touch (1)(blue) is touched?</span>, and another Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">broadcast ()</span>. Create a new piece of message "blue", so that when the blue alligator clip is touched, the message named of the same color will be broadcast.

<img src="images/funny-touch-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. Likewise, we can create three new pieces of message, namely "yellow", "red", and "green", for corresponding alligator clips. Simply duplicate the script for the blue alligator clip.

<img src="images/funny-touch-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. Add a Control block <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">forever</span>

<img src="images/funny-touch-4.gif" style="padding:3px 0px 15px 0px; width:800px;">

### Program the sprite

4\. Select "Sprites", and delete the default sprite "Panda".

<img src="images/funny-touch-5.gif" style="padding:3px 0px 15px 0px; width:800px;">

5\. Click "+" to add a new sprite "Max".

<img src="images/funny-touch-6.gif" style="padding:3px 0px 15px 0px; width:800px;">

6\. Click "+ extension" at the bottom of the Blocks Area. We'll use the "Music" extension.

<img src="images/funny-touch-7.gif" style="padding:3px 0px 15px 0px; width:800px;">

7\. Add an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when I receive (blue)</span>, and a Music block <span style="background-color:#0FBD8C;color:white;padding:3px; font-size: 12px">play drum ((1)Snare Drum) for (0.25) beats</span>.

<img src="images/funny-touch-8.gif" style="padding:3px 0px 15px 0px; width:800px;">

7\. Duplicate the script. When message "yellow", "red", or "green" is received, drum "Bass Drum", "Side Stick", or "Crash Cymbal" will be played.

<img src="images/funny-touch-9.gif" style="padding:3px 0px 15px 0px; width:800px;">

9\. Click the green flag. See what happens?

<img src="../../zh/examples/images/funny-touch-00.png" style="padding:3px 0px 15px 0px;">
