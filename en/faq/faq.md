<img src="../../zh/faq/images/neuron-all.jpg" style="padding:3px 3px 15px 0px;">

# FAQ for Neuron

## 1. I purchased a Neuron Inventor Kit. Are all of the blocks compatible with Codey Rocky?

The Neuron Inventor Kit includes the following blocks:
- Funny touch
- LED panel
- Buzzer
- Servo
- Gyroscope

Only the **LED Panel** block and the **Buzzer** block are compatible with Codey Rocky. In the later version of mBlock 5, more Neuron blocks will be available. Stay tuned!

---

## 2. I purchased a Creative Lab Kit. Are all the blocks included compatible with Codey Rocky?

Currently, only the following blocks are compatible with Codey Rocky.    In the later version of mBlock 5, more Neuron blocks will be available. Stay tuned!

- LED panel
- LED strip
- Buzzer
- Ultrasonic sensor
- PIR sensor
- Humiture sensor

---

## 3. Why are the El wire/motor/servo/LED panel blocks not compatible with both Neuron and Codey Rocky?

In the mBlock 5 Beta 3.1, only the **LED panel** block is compatible with Codey Rocky.

---