# Accessories

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="accessory/laser-pointer.html" target="_blank"><img src="../../zh/hardware/accessory/images/laser-pointer.jpg" width="250px;"></a><br>
<p>Laser Pointer Block</p></td>

<td width="33%;"><a href="accessory/neuron-board.html" target="_blank"><img src="../../zh/hardware/accessory/images/neuron-board.jpg" width="250px;"></a><br>
<p>Neuron Board</p></td>

<td width="33%;"><a href="accessory/magnet-wire.html" target="_blank"><img src="../../zh/hardware/accessory/images/magnet-wire.jpg" width="250px;"></a><br>
<p>Magnet Wire</p></td>
</tr>

<tr>
<td><a href="accessory/usb-cable.html" target="_blank"><img src="../../zh/hardware/accessory/images/usb-cable.jpg" width="250px;"></a><br>
<p>USB Cable</p></td>
<td><a href="accessory/clip-and-rubber-band.html" target="_blank"><img src="../../zh/hardware/accessory/images/clip-rubber-band-2.jpg" width="250px;"></a><br>
<p>Fixed Clips(16) & Rubber Bands(80)</p></td>
</tr>
</table>