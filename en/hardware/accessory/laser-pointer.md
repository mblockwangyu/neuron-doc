<img src="../../../zh/hardware/accessory/images/laser-pointer-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/accessory/images/laser-pointer-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/accessory/images/laser-pointer-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/accessory/images/laser-pointer-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Laser Pointer Block

The Laser Pointer emits a laser beam, and works together with the Light Sensor to detect objects passing by.

- Net weight: 10.8g
- Volume: 24×24×27mm

### Parameters

- Laser Transmit Power: 1mW
- Battery Capacity: 85mAh   
- Life Time: ≥10h
- Charging Time: ≈100min
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Rechargeable.
- Long battery life.
- Custom-designed Laser Pointer block that does not cause eye damage.

