<img src="../../../zh/hardware/accessory/images/magnet-wire-10cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/accessory/images/magnet-wire-20cm-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/accessory/images/magnet-wire-10cm-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/accessory/images/magnet-wire-20cm-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">


# Magnet Wire 

The Magnet Wire provides a more flexible way of connecting blocks. There are two choices: 10cm, or 20cm.

**Magnet Wire (10cm)**

- Net weight: 10.3g
- Volume: 100×24×12mm

**Magnet Wire (20cm)**

- Net weight: 12.1g
- Volume: 200×24×12mm

### Parameters

- Connection Lifespan: ＞6000 cycles

### Features

- Safe skin-friendly material.
- Fine appearance.

