<img src="../../../zh/hardware/accessory/images/neuron-board-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/accessory/images/neuron-board-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/accessory/images/neuron-board-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/accessory/images/neuron-board-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Neuron Board

Contains 6 Neuron Boards and 36 friction pin connectors.

- Net weight: 93.3
- Volume: 48×48×8.3mm

### Parameters

- Material: ABS

### Features

- Compatible with LEGO® blocks.
- Compatible with the Makeblock platform.
- Magnetic adsorption.
- Combine blocks freely to create hundreds of inventions.

					
