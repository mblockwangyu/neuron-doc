<img src="../../../zh/hardware/accessory/images/usb-cable-20cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/accessory/images/usb-cable-100cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">


# USB Cable 

The USB Cable can be used in charging or data transmission. There are two choices: 20cm, or 100cm.

**USB Cable (20cm)**

- Net weight: 9.6g
- Volume: 20cm

**USB Cable (100cm)**

- Net weight: 25.6g
- Volume: 100cm

### Parameters

- Connection Lifespan: ＞3000 cycles

### Features

Supports data transmission.