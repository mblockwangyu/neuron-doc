# Hardware

### Power and Communication

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="power-and-communication/power.html" target="_blank"><img src="../../zh/hardware/power-and-communication/images/power.jpg" width="250px;"></a><br>
<p>Power</p></td>

<td width="33%;"><a href="power-and-communication/bluetooth.html" target="_blank"><img src="../../zh/hardware/power-and-communication/images/bluetooth.jpg" width="250px;"></a><br>
<p>Bluetooth</p></td>

<td width="33%;"><a href="power-and-communication/wireless.html" target="_blank"><img src="../../zh/hardware/power-and-communication/images/wireless.jpg" width="250px;"></a><br>
<p>Wireless Transmitter and Receiver</p></td>
</tr>

<tr>


<td><a href="power-and-communication/wifi.html" target="_blank"><img src="../../zh/hardware/power-and-communication/images/wifi.jpg" width="250px;"></a><br>
<p>Wi-Fi</p></td>
</tr>
</table>

### Input Blocks

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="input/joystick.html" target="_blank"><img src="../../zh/hardware/input/images/joystick.jpg" width="250px;"></a><br>
<p>Joystick</p></td>

<td width="33%;"><a href="input/button.html" target="_blank"><img src="../../zh/hardware/input/images/button.jpg" width="250px;"></a><br>
<p>Button</p></td>

<td width="33%;"><a href="input/knob.html" target="_blank"><img src="../../zh/hardware/input/images/knob.jpg" width="250px;"></a><br>
<p>Knob</p></td>
</tr>

<tr>
<td><a href="input/touch.html" target="_blank"><img src="../../zh/hardware/input/images/touch.jpg" width="250px;"></a><br>
<p>Funny Touch</p></td>
<td><a href="input/light-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/light-sensor.jpg" width="250px;"></a><br>
<p>Light Sensor</p></td>
<td><a href="input/dual-ir.html" target="_blank"><img src="../../zh/hardware/input/images/dual-ir.jpg" width="250px;"></a><br>
<p>Dual IR Detector</p></td>
</tr>

<tr>
<td><a href="input/ultrasonic.html" target="_blank"><img src="../../zh/hardware/input/images/ultrasonic.jpg" width="250px;"></a><br>
<p>Ultrasonic Sensor</p></td>
<td><a href="input/sound-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/sound-sensor.jpg" width="250px;"></a><br>
<p>Sound Sensor</p></td>
<td><a href="input/pir-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/pir-sensor.jpg" width="250px;"></a><br>
<p>PIR Sensor</p></td>
</tr>

<tr>
<td><a href="input/camera.html" target="_blank"><img src="../../zh/hardware/input/images/camera.jpg" width="250px;"></a><br>
<p>Camera</p></td>
<td><a href="input/color-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/color-sensor.jpg" width="250px;"></a><br>
<p>Color Sensor</p></td>
<td><a href="input/gyro.html" target="_blank"><img src="../../zh/hardware/input/images/gyro.jpg" width="250px;"></a><br>
<p>Gyro Sensor</p></td>
</tr>

<tr>
<td><a href="input/temperature-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/temperature-sensor.jpg" width="250px;"></a><br>
<p>Temperature Sensor</p></td>
<td><a href="input/humiture-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/humiture-sensor.jpg" width="250px;"></a><br>
<p>Humiture Sensor</p></td>
<td><a href="input/soil-moisture-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/soil-moisture.jpg" width="250px;"></a><br>
<p>Soil Moisture Sensor</p></td>
</tr>

</table>

### Output Blocks

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="output/rgb-led.html" target="_blank"><img src="../../zh/hardware/output/images/led.jpg" width="250px;"></a><br>
<p>RGB LED</p></td>

<td width="33%;"><a href="output/el-wire-driver.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-driver.jpg" width="250px;"></a><br>
<p>EL Wire Driver Block</p></td>

<td width="33%;"><a href="output/el-wire-1.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-a-0.jpg" width="250px;"></a><br>
<p>EL Wire Package A (Red/Green/Yellow/Orange)</p></td>
</tr>

<tr>
<td><a href="output/el-wire-2.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-b-0.jpg" width="250px;"></a><br>
<p>EL Wire Package B (Blue/Purple/Pink/White)</p></td>
<td><a href="output/buzzer.html" target="_blank"><img src="../../zh/hardware/output/images/buzzer.jpg" width="250px;"></a><br>
<p>Buzzer Block</p></td>
<td><a href="output/dual-dc-motor.html" target="_blank"><img src="../../zh/hardware/output/images/dual-dc-motor-driver.jpg" width="250px;"></a><br>
<p>Dual DC Motor Driver Block</p></td>
</tr>

<tr>
<td><a href="output/dc-motor.html" target="_blank"><img src="../../zh/hardware/output/images/dc-motor.jpg" width="250px;"></a><br>
<p>DC Motor Package</p></td>
<td><a href="output/water-pump.html" target="_blank"><img src="../../zh/hardware/output/images/water-pump.jpg" width="250px;"></a><br>
<p>Water Pump Package</p></td>
<td><a href="output/dual-servo-driver.html" target="_blank"><img src="../../zh/hardware/output/images/dual-servo-driver.jpg" width="250px;"></a><br>
<p>Dual Servo Driver Block</p></td>
</tr>

<tr>
<td><a href="output/servo.html" target="_blank"><img src="../../zh/hardware/output/images/servo.jpg" width="250px;"></a><br>
<p>Servo Package</p></td>
<td><a href="output/led-panel.html" target="_blank"><img src="../../zh/hardware/output/images/led-panel.jpg" width="250px;"></a><br>
<p>LED Panel Block</p></td>
<td><a href="output/led-strip-driver.html" target="_blank"><img src="../../zh/hardware/output/images/led-strip-driver.jpg" width="250px;"></a><br>
<p>LED Strip Driver Block</p></td>
</tr>

<tr>
<td><a href="output/led-strip.html" target="_blank"><img src="../../zh/hardware/output/images/led-strip.jpg" width="250px;"></a><br>
<p>LED Strip (0.5m)</p></td>
<td><a href="output/display.html" target="_blank"><img src="../../zh/hardware/output/images/display.jpg" width="250px;"></a><br>
<p>Display Block</p></td>
<td><a href="output/speaker.html" target="_blank"><img src="../../zh/hardware/output/images/speaker.jpg" width="250px;"></a><br>
<p>Mic & Speaker Block</p></td>
</tr>
</table>

### Accessories

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="accessory/laser-pointer.html" target="_blank"><img src="../../zh/hardware/accessory/images/laser-pointer.jpg" width="250px;"></a><br>
<p>Laser Pointer Block</p></td>

<td width="33%;"><a href="accessory/neuron-board.html" target="_blank"><img src="../../zh/hardware/accessory/images/neuron-board.jpg" width="250px;"></a><br>
<p>Neuron Board</p></td>

<td width="33%;"><a href="accessory/magnet-wire.html" target="_blank"><img src="../../zh/hardware/accessory/images/magnet-wire.jpg" width="250px;"></a><br>
<p>Magnet Wire</p></td>
</tr>

<tr>
<td><a href="accessory/usb-cable.html" target="_blank"><img src="../../zh/hardware/accessory/images/usb-cable.jpg" width="250px;"></a><br>
<p>USB Cable</p></td>
<td><a href="accessory/clip-and-rubber-band.html" target="_blank"><img src="../../zh/hardware/accessory/images/clip-rubber-band-2.jpg" width="250px;"></a><br>
<p>Fixed Clips(16) & Rubber Bands(80)</p></td>
</tr>
</table>