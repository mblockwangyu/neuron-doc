# Input Blocks

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="input/joystick.html" target="_blank"><img src="../../zh/hardware/input/images/joystick.jpg" width="250px;"></a><br>
<p>Joystick</p></td>

<td width="33%;"><a href="input/button.html" target="_blank"><img src="../../zh/hardware/input/images/button.jpg" width="250px;"></a><br>
<p>Button</p></td>

<td width="33%;"><a href="input/knob.html" target="_blank"><img src="../../zh/hardware/input/images/knob.jpg" width="250px;"></a><br>
<p>Knob</p></td>
</tr>

<tr>
<td><a href="input/touch.html" target="_blank"><img src="../../zh/hardware/input/images/touch.jpg" width="250px;"></a><br>
<p>Funny Touch</p></td>
<td><a href="input/light-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/light-sensor.jpg" width="250px;"></a><br>
<p>Light Sensor</p></td>
<td><a href="input/dual-ir.html" target="_blank"><img src="../../zh/hardware/input/images/dual-ir.jpg" width="250px;"></a><br>
<p>Dual IR Detector</p></td>
</tr>

<tr>
<td><a href="input/ultrasonic.html" target="_blank"><img src="../../zh/hardware/input/images/ultrasonic.jpg" width="250px;"></a><br>
<p>Ultrasonic Sensor</p></td>
<td><a href="input/sound-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/sound-sensor.jpg" width="250px;"></a><br>
<p>Sound Sensor</p></td>
<td><a href="input/pir-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/pir-sensor.jpg" width="250px;"></a><br>
<p>PIR Sensor</p></td>
</tr>

<tr>
<td><a href="input/camera.html" target="_blank"><img src="../../zh/hardware/input/images/camera.jpg" width="250px;"></a><br>
<p>Camera</p></td>
<td><a href="input/color-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/color-sensor.jpg" width="250px;"></a><br>
<p>Color Sensor</p></td>
<td><a href="input/gyro.html" target="_blank"><img src="../../zh/hardware/input/images/gyro.jpg" width="250px;"></a><br>
<p>Gyro Sensor</p></td>
</tr>

<tr>
<td><a href="input/temperature-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/temperature-sensor.jpg" width="250px;"></a><br>
<p>Temperature Sensor</p></td>
<td><a href="input/humiture-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/humiture-sensor.jpg" width="250px;"></a><br>
<p>Humiture Sensor</p></td>
<td><a href="input/soil-moisture-sensor.html" target="_blank"><img src="../../zh/hardware/input/images/soil-moisture.jpg" width="250px;"></a><br>
<p>Soil Moisture Sensor</p></td>
</tr>

</table>