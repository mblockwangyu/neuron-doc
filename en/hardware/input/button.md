<img src="../../../zh/hardware/input/images/button-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/button-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/button-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/button-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Button Block

The Button works as a switch to open and close your invention.

- Net weight: 7.7g
- Volume: 24×24×21mm

### Parameters

- Life span: >10000 cycles
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Long press-trigger, up to 5 seconds.
- Contains the independent status indicator light and main control chip.
- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.
- Supports two ways to update firmware: wired and wireless.
- The modules are color-coded according to their functionality, to make it easy to distinguish between them and find the right block. 
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.
- The sides of each block are covered by a silicone sleeve to make sure that there are no sharp corners exposed.

