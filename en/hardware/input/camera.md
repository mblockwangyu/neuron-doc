<img src="../../../zh/hardware/input/images/camera-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/camera-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/camera-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/camera-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Camera Block

The Camera block adds photographic capability to your invention.	

- Net weight: 8.1g
- Volume: 24×24×22mm

### Parameters

- Resolution: 1280×720   
- Pixel size: 3.4um×3.4um
- Maximum image transfer rate: full size @ 30fps
- Optical FOV: 100°
- Lens construction: 4G+IR   
- F/No: 2.97
- Effective focal length(EFL): 2.4mm
- Power Consumption: 100uA (standby)~240mW (active)
- Power Supply: USB Bus Power    
- Anti-drop ability: 1m
- Operating temperature: -30℃~70℃ (operating), 0℃~50℃ (stable image)
- Operating humidity: < 95%

### Features

- HD resolution.
- Supports AI face and voice recognition. 
- Supports offline operation when connected to Wi-Fi block.

