<img src="../../../zh/hardware/input/images/color-sensor-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/color-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/color-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/color-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Color Sensor

The color sensor detects the color of an object, and returns a group of RGB values.

- Net weight: 7.3g
- Volume: 24×24×14mm

### Parameters

- Refresh rate: 25Hz
- Dynamic range: 3800000:1 
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- High sensitivity
- Supports color picking function with the software

