<img src="../../../zh/hardware/input/images/dual-ir-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/dual-ir-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/dual-ir-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/dual-ir-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Dual IR Detector

The dual IR detector includes two pairs of infrared reflective photoelectric sensors, which can work separately as buttons, or be used to detect black lines on the ground.

- Net weight: 11.2g
- Volume: 24×48×14mm

### Parameters

- Measuring range: 0~2cm
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"	

### Features

Specially designed for Neuron. An exclusive module for Neuron among all Makeblock STEAM Kits.


