<img src="../../../zh/hardware/input/images/gyro-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/gyro-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/gyro-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/gyro-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Gyro Sensor

The gyro detects the acceleration or angles of the object, returns corresponding values. The gyro can also be used to detect shaking or vibration of the object.

- Net weight: 7.4g
- Volume: 24×24×14mm

### Parameters

- Pitch angle: ±180°
- Roll angle: ±90°
- Acceleration: ±8g 
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Supports attitude angle detection
- Able to detect a variety of intensity of vibration or shaking.
