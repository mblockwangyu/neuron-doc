<img src="../../../zh/hardware/input/images/humiture-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/humiture-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/humiture-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/humiture-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Humiture Sensor Block

The Humiture Sensor detects the temperature and humidity of the surrounding area.

- Net weight: 7.5g
- Volume: 24×24×14mm

### Parameters

- Temperature Range: -40℃~125℃
- Temperature Accuracy: ±1℃
- Humidity Range: 0%~100%
- Humidity Accuracy: ±3%
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m

### Features

- Detects temperature and humidity simultaneously.

### Precautions

The block is not waterproof. Please avoid water while using.