<img src="../../../zh/hardware/input/images/knob-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/knob-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/knob-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/knob-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Knob Block

Use the Knob to increase or decrease output, to control brightness, sound frequency, or speed.

- Net weight: 8.2g
- Volume: 24×24×22.5mm

### Parameters

- Life span: >10000 cycles
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Metal knob block with greatly improved durability.
