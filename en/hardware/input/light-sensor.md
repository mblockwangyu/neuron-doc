<img src="../../../zh/hardware/input/images/light-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/light-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/light-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/light-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Light Sensor Block

The light sensor block detects ambient light intensity. The brighter the light is, the stronger the output signal will be.

- Net weight: 7.3g
- Volume: 24×24×14mm

### Parameters

- Measuring range: 0~100 (relative value)
- Measurement accuracy: ±5
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%
