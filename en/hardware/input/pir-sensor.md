<img src="../../../zh/hardware/input/images/pir-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/pir-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/pir-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/pir-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# PIR Sensor Block

The Passive Infrared (PIR) sensor detects motion from humans within 3 meters. If moves are detected within that range, the sensor will send an 'ON' signal to the following blocks in the chain.

- Net weight: 8.7g
- Volume: 24x24x25.6mm

### Parameters

- Measuring range: 0~3m
- Detecting angle: 120°
- Initialization time: < 5s
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"
