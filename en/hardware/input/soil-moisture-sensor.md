<img src="../../../zh/hardware/input/images/soil-moisture-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/soil-moisture-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/soil-moisture-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/soil-moisture-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Soil Moisture Sensor Block

The Soil Moisture Sensor detects the level of moisture in the soil.

- Net weight: 17.2g
- Volume: 24×24×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Precautions

Soil contain a certain corrosive substance. Please do not put the probe of the sensor into soil for a long time.
						
