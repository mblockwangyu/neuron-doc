<img src="../../../zh/hardware/input/images/sound-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/sound-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/sound-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/sound-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Sound Sensor Block

The Sound Sensor detects sound intensity. The stronger the sound, the stronger the signal sent out is.

- Net weight: 7.5g
- Volume: 24×24×14mm

### Parameters

- Measuring range: 0~100 (relative value)
- Measurement accuracy: ±5
- Sensitivity: -38±3dB
- Direction: omni-directional
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"


