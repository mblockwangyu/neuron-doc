<img src="../../../zh/hardware/input/images/temperature-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/temperature-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/temperature-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/temperature-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Temperature Sensor Block

The Temperature Sensor can detect the temperature of water, a body, and other objects.

- Net weight: 13.7g
- Volume: 24×24×14mm

### Parameters

- Operating voltage: 5V
- Measuring range: -55℃~125℃
- Measurement accuracy: ±0.5℃（-10℃~85℃）
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"	

### Features

- High sensitivity	

### Precautions

The sensor wire would be damaged when the measuring temperature is over 100℃. Please be careful to avoid damage.
