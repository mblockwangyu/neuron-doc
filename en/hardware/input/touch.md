<img src="../../../zh/hardware/input/images/touch-2.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/touch-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/touch-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/touch-4.png" width="200px;" style="padding:3px 0px 12px 0px;">


# Funny Touch

Funny Touch can turn any conductive object (such as a banana, or water) into a touch switch. By detecting the conductive state of the four switches and GND wire, you can add simple human-computer interactivity to your invention.

- Net weight: 28.8g
- Volume: 24×48×14mm

### Parameters

- Trigger range: < 24MΩ 
- Clip size: 35mm
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%	

### Features

- 16 types of input combinations.
- Rich offline interaction with other blocks.
- Multiple triggering methods.
- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.
- Supports two ways to update firmware: wired and wireless.
The modules are color-coded according to their functionality, to make it easy to distinguish between them and find the right block. 
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.
- The sides of each block are covered by a silicone sleeve to make sure that there are no sharp corners exposed.
Customized plain top alligator clips for increased safety.


### Precaution

The alligator clip is sharp, please do not use it to clip yourself or others to avoid causing injury.
					
					
