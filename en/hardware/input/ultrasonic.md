<img src="../../../zh/hardware/input/images/ultrasonic-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/input/images/ultrasonic-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/ultrasonic-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/input/images/ultrasonic-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Ultrasonic Sensor Block

The ultrasonic sensor block can be used to measure the distance between the block and an obstacle, from 3 cm to 300 cm.	

- Net weight: 12.4g
- Volume: 24×48×14mm

### Parameters

- Measuring range: 3~300cm
- Measurement accuracy: ±5%
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

A unique block only available on the Makeblock Neuron platform.