# Output Blocks

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="output/rgb-led.html" target="_blank"><img src="../../zh/hardware/output/images/led.jpg" width="250px;"></a><br>
<p>RGB LED</p></td>

<td width="33%;"><a href="output/el-wire-driver.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-driver.jpg" width="250px;"></a><br>
<p>EL Wire Driver Block</p></td>

<td width="33%;"><a href="output/el-wire-1.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-a-0.jpg" width="250px;"></a><br>
<p>EL Wire Package A (Red/Green/Yellow/Orange)</p></td>
</tr>

<tr>
<td><a href="output/el-wire-2.html" target="_blank"><img src="../../zh/hardware/output/images/el-wire-b-0.jpg" width="250px;"></a><br>
<p>EL Wire Package B (Blue/Purple/Pink/White)</p></td>
<td><a href="output/buzzer.html" target="_blank"><img src="../../zh/hardware/output/images/buzzer.jpg" width="250px;"></a><br>
<p>Buzzer Block</p></td>
<td><a href="output/dual-dc-motor.html" target="_blank"><img src="../../zh/hardware/output/images/dual-dc-motor-driver.jpg" width="250px;"></a><br>
<p>Dual DC Motor Driver Block</p></td>
</tr>

<tr>
<td><a href="output/dc-motor.html" target="_blank"><img src="../../zh/hardware/output/images/dc-motor.jpg" width="250px;"></a><br>
<p>DC Motor Package</p></td>
<td><a href="output/water-pump.html" target="_blank"><img src="../../zh/hardware/output/images/water-pump.jpg" width="250px;"></a><br>
<p>Water Pump Package</p></td>
<td><a href="output/dual-servo-driver.html" target="_blank"><img src="../../zh/hardware/output/images/dual-servo-driver.jpg" width="250px;"></a><br>
<p>Dual Servo Driver Block</p></td>
</tr>

<tr>
<td><a href="output/servo.html" target="_blank"><img src="../../zh/hardware/output/images/servo.jpg" width="250px;"></a><br>
<p>Servo Package</p></td>
<td><a href="output/led-panel.html" target="_blank"><img src="../../zh/hardware/output/images/led-panel.jpg" width="250px;"></a><br>
<p>LED Panel Block</p></td>
<td><a href="output/led-strip-driver.html" target="_blank"><img src="../../zh/hardware/output/images/led-strip-driver.jpg" width="250px;"></a><br>
<p>LED Strip Driver Block</p></td>
</tr>

<tr>
<td><a href="output/led-strip.html" target="_blank"><img src="../../zh/hardware/output/images/led-strip.jpg" width="250px;"></a><br>
<p>LED Strip (0.5m)</p></td>
<td><a href="output/display.html" target="_blank"><img src="../../zh/hardware/output/images/display.jpg" width="250px;"></a><br>
<p>Display Block</p></td>
<td><a href="output/speaker.html" target="_blank"><img src="../../zh/hardware/output/images/speaker.jpg" width="250px;"></a><br>
<p>Mic & Speaker Block</p></td>
</tr>
</table>