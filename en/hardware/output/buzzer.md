<img src="../../../zh/hardware/output/images/buzzer-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/buzzer-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/buzzer-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/buzzer-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Buzzer Block

The Buzzer emits sound when it receives an 'ON' signal or code instructions. It can be used to construct analog alarms, doorbells, and other devices with sound features.

- Net weight: 7.9g
- Volume: 24×24×15mm

### Parameters

- Frequency Range:0~20000Hz
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"
