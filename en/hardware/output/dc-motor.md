<img src="../../../zh/hardware/output/images/dc-motor-5.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/dc-motor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/dc-motor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/dc-motor-5.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# DC Motor Package

The DC (Direct Current) motor requires a DC motor driver to work. The DC motor can span, and the speed and direction of the spanning can be controlled through programming  (mBlock 5 or Neuron APP). You can use the DC motor to build moving vehicles, mechanical arms, catapults, and others.

- Net weight: 49.9g

### Parameters

- Rated speed: 118±10%RPM
- Gear ratio: 1:100
- Rated torque: 132g/cm 
- Stall torque: 590±100g/cm 
- Rated current: 0.1A    
- Stall current: 0.4A     

### Features

Contains LEGO® cross adaptor and the standard wheel from the Makeblock platform. Compatible with both Makeblock platform and LEGO®.