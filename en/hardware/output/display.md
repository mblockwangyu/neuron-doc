<img src="../../../zh/hardware/output/images/display-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/display-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/display-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/display-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Display Block

The display shows the input value. You can program to customize the displayed message, such as number, emojis, images, etc.

- Net weight: 12.4g
- Volume: 24×48×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

Displays content like numbers, images, emojis, and more.
