<img src="../../../zh/hardware/output/images/dual-servo-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/dual-servo-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/dual-servo-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/dual-servo-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Dual Servo Driver Block

The Dual Servo Driver can drive two servos simultaneously.

- Net weight: 12g	
- Volume: 24×48×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

Drives two servos simultaneously.
