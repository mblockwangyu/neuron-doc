<img src="../../../zh/hardware/output/images/el-wire-b-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/el-wire-b-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">


# EL Wire Package B (Blue Purple Pink White)

Contains four colors of EL Wire: blue, purple, pink, and white. Requires the EL Wire Driver block.

- Net weight: 32.5g
- Volume: 1000mm

### Parameters

- Light Intensity: 100nits
- Outer Material:  PVC
- Tensile Strength: < 1kg
- Lifespan: ≥5000h
- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Precautions

There would be a slight noise when EL wire driver sensor is working. It is normal and does not affect performance.