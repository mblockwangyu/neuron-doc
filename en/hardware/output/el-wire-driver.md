<img src="../../../zh/hardware/output/images/el-wire-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/el-wire-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/el-wire-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/el-wire-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# EL Wire Driver Block

The EL Wire Driver can light up EL wire of four different colors simultaneously. You can use it with a connecting rod to make more creations.

- Net weight: 17.2g
- Volume: 24×48×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

Lights up EL wire of four different colors simultaneously.
