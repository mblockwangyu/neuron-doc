<img src="../../../zh/hardware/output/images/led-panel-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/led-panel-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-panel-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-panel-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# LED Panel Block

The LED Panel contains 64 (8×8) RGB LED lights and can display emojis, text, animations, etc.

- Net weight: 20.1g
- Volume: 48×48×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Displays up to 256 colors.
- Supports scrolling display of English letters and digits.

