<img src="../../../zh/hardware/output/images/led-strip-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/led-strip-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-strip-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-strip-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# LED Strip Driver Block

The LED Strip Driver powers the LED Strip.

- Net weight: 7.5g
- Volume: 24×48×14mm

### Parameters

- Operating voltage:  DC 5V
- Anti-drop ability:  1.5m
- Operating temperature: -10℃~55℃
- Operating humidity:  < 95%

### Features

Multiple preset lighting effects.
