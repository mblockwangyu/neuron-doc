<img src="../../../zh/hardware/output/images/led-strip-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/led-strip-1.jpg" width="250px;" style="padding:3px 5px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-strip-2.jpg" width="250px;" style="padding:3px 0px 12px 0px;">


# LED Strip (0.5m)

The LED strip works with the LED Strip Driver. The LED Strip contains 15 RGB LEDs, each of which can be programmed independently to emit a specified color.

- Net weight: 20g
- Volume: 500mm

### Features

Contains 15 RGB LEDs, each of which can be programmed independently.


