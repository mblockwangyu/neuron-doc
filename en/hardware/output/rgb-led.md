<img src="../../../zh/hardware/output/images/led-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/led-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/led-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# RGB LED Block

The RGB led can emit light with different color or lightness, according to received signal or programming command. You can make various creations, such as a traffic light, a flashlight, a night light, and the like.

- Net weight: 7.3g
- Volume: 24×24×14mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

Displays up to 256 colors.
