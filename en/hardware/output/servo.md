<img src="../../../zh/hardware/output/images/servo-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/servo-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/servo-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/servo-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Servo Package

The servo works with the servo driver, and can rotate from 0 to 180 degrees. You can program (mBlock 5 or Neuron APP) to control the degree of rotation.

- Net weight: 24.5

### Parameters

- Limit angle: 180°±10°
- Excessive play: ≤1° 
- Stall torque (at locked): 1.5±0.05kg·cm 
- Idle current (at stopped)6±1mA
- Operating voltage: 4.8V~6.0V 
- Operating temperature: -10℃-50℃
- Storage temperature: -20℃~60℃

### Features

Rich Accessories. Compatible with Makeblock platform and LEGO®.
