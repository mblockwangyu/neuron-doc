<img src="../../../zh/hardware/output/images/speaker-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/speaker-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/speaker-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/speaker-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Mic & Speaker Block

Use the Mic & Speaker block to play a sound, or to record your voice.

- Net weight: 24.5g
- Volume: 48×48×13mm

### Parameters

- Operating voltage: DC 5V
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- Stores your record.
- Multiple built-in sound effects.
- Supports AI voice recognition via the Makeblock Neuron app.
- Supports offline operation when connected to Wi-Fi block.

