<img src="../../../zh/hardware/output/images/water-pump-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/output/images/water-pump-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/water-pump-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/output/images/water-pump-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Water Pump Package

The Water Pump can move water to let you create inventions powered by potential energy.

- Net weight: 91.2
- Volume: D27×75mm

### Parameters

- Water hole diameter: 6.5mm
- Noise: < 60dB
- Rated voltage: 12V   
- Current (with load): < 320mA  
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

Low working noise.
