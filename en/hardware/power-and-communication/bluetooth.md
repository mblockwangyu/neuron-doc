<img src="../../../zh/hardware/power-and-communication/images/bluetooth-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/power-and-communication/images/bluetooth-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/bluetooth-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/bluetooth-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Bluetooth Block

The bluetooth block adds wireless Bluetooth connectivity to your blocks, and can also power to other blocks when connected to a power bank or power source via USB.

- Net weight: 11.3g	
- Volume: 24×48×14mm

### Parameters

- Bluetooth version:BT4.0
- Frequency range: 2402~2480MHz
- Antenna gain: 1.5dBi
- Energy consumption: ≤4dBm
- FCC ID：2AH9Q-NU001BT
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%

### Features

- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.	
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.	
- Supports serial port communication.	
- Charges other blocks.	
