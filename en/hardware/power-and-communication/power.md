<img src="../../../zh/hardware/power-and-communication/images/power-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/power-and-communication/images/power-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/power-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/power-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Power Block

Powers the other blocks. Connect and press to power on. Press and hold for 3 seconds to power off.

- Net weight: 39.4g
- Volume: 48×48×14mm


### Parameters

- Battery capacity: 950mAh
- Capacity: maximum 4 hours
- Charging time: ≈75分钟
- Operating Voltage: DC 5V   
- Input: < 1A 
- Anti-drop ability: 1.5m
- Operating temperature: 0℃~45℃ 
- Storage temperature: -10℃~55℃ 
- Operating humidity: < 95%
- APROM: 32KB
- RAM: 4KB
- Interface Type: Magnetic Pogo Pin
- Connection Lifespan: ＞100000 cycles
- MCU: mini58 (32-bit)

### Features

- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.	
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.	
- Real-time power display.	
- Automatically powers off after the block is disconnected, extending battery life.	
- Includes charging protection.	


### Precautions

- Recharge at least once every three months
- Do not power other modules while charging
- Actual capacity depends on the number of connected modules. The value provided is under ideal test environment, for reference only.
- The battery capacity decreases to some extent after repeated charging, determined by the characteristics of the lithium battery. After 300 times of charging, the battery capacity will remain at least 80% of the initial value.