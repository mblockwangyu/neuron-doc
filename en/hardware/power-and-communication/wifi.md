<img src="../../../zh/hardware/power-and-communication/images/wifi-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/power-and-communication/images/wifi-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/wifi-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/wifi-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Wi-Fi Block

Give your blocks Wi-Fi connectivity to control them with your tablet, or connect to the Makeblock Creative Cloud and Internet of Things-devices.

- Net weight: 25.7g
- Volume: 48×48×14mm	

### Parameters

- Wireless Standards: IEEE802.11b/g/n(HT20)
- Frequency range: 2412~2462MHz
- Working Mode: STA/AP/STA+AP
- Communication Distance: 10m (Open environment)
- Operating voltage: DC 5V    
- Operating Current: 200mA
- FCC ID：2AH9Q-NU001WF
- Anti-drop ability: 1.5m
- Operating temperature: 0℃~45℃
- Operating humidity: < 95%

### Features

- Can be programmed offline 
- Supports Internet of Things (IoT) when connected to the internet.
- Can be directly connected to the app by using the Wi-Fi block.
- Contains the independent status indicator light and main control chip.
- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.
- Supports two ways to update firmware: wired and wireless.
- The modules are color-coded according to their functionality, to make it easy to distinguish between them and find the right block. 
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.
- The sides of each block are covered by a silicone sleeve to make sure that there are no sharp corners exposed.


### Precautions

- Please ensure the Wi-Fi block has a good power supply environment.
- It takes some time for the Wi-Fi block to boot (30~60s), please be patient.

