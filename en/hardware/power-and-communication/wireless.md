<img src="../../../zh/hardware/power-and-communication/images/wireless-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="../../../zh/hardware/power-and-communication/images/wireless-2.jpg" width="300px;" style="padding:3px 0px 12px 0px;">
<img src="../../../zh/hardware/power-and-communication/images/wireless-3.jpg" width="300px;" style="padding:3px 0px 12px 0px;">


# Wireless Transmitter Block &  Wireless Receiver Block 

These two blocks let you establish a stand-alone connection between two chains of blocks so that they can communicate wirelessly. Maximum range: 10m.

- Net weight: 22.4g
- Volume: 24×48×14mm

### Parameters

- Bluetooth version:BT4.0
- Frequency range: 2402~2480MHz
- Antenna gain: 1.5dBi
- Communication range: 10m
- Energy consumption: ≤4dBm
- FCC ID：2AH9Q-NU001WT
- Anti-drop ability: 1.5m
- Operating temperature: -10℃~55℃
- Operating humidity: < 95%"

### Features

- The magnetic design at the back of Neuron blocks makes you can simply place your gadget on the magnetic surface such as whiteboard.	
- The redesigned magnetic Pogo Pin-connectors effectively prevent blocks from being connected incorrectly.	
- Pairs with other blocks through a physical connection.	


### Precautions

Pairing takes time. After pairing is done, and the wireless blocks are reconnected to other blocks, the connection still takes some time (usually < 10s). Please be patient.

