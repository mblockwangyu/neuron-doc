# Up on the Cloud

In the “Control” tab of the Node Shelf, you can create nodes such as Buttons or Switches. These Buttons and Switches are unlike physical Neuron Blocks. You can interact with them on the iPad Screen.
 
<img src="images/cloud-1.png" width="600px;" style="padding:30px 3px 27px 0px;">

<img src="images/cloud-2.png" width="600px;" style="padding:30px 3px 27px 0px;">

These nodes under “Control” tab has a second function. They form a control panel you can access through the button at the right top of the screen. At the control panel, it is possible to create a link and QR code using the “Share” button. Then you may open the link in your cell phone, to control your project from remote places.

<img src="images/cloud-3.png" width="600px;" style="padding:30px 3px 27px 0px;">