# Setup Microsoft Cognitive Service

Neuron can utilize computer vision, emotion recognition, speech recognition and other AI features with Microsoft cognitive services. Before use, you need to setup your Wifi block’s Internet connection. The instructions are listed here. Furthermore, you need to register a “key” from Microsoft in order to use features like computer vision, emotional recognition, voice recognition. This is an example on how to set up the key needed for voice recognition.

After that, you need to connect a “Mic & Speaker” block to the Wifi Block through its usb ports . Then you will see the “Sound” tab popping up at the Node Shelf on the bottom of the screen.

In this example, drag a Speech node from the “Sound” tab to the platform, tap the just-added speech node, then click the “SET” button to open the config panel. Then you should see a screen like this one:

<img src="images/cognitive-1.png" width="600px;" style="padding:30px 3px 27px 0px;">

When you click the “SET” button, the following dialog will pop up, showing three types of Key, including Computer Vision, Emotion, Bing Speech, Each type has a “LINK” button, click on the button will jump to the corresponding URL in which you will be able to get the corresponding key. But before that, it is necessary to understand how to get these keys, so here we strongly recommend you to click “How to find these API keys” to find out the relevant content, showing as below:

<img src="images/cognitive-2.png" width="600px;" style="padding:30px 3px 27px 0px;">

After learning how to get the corresponding key, let’s return to the dialog showing below, and take Bing Speech as an example to get its corresponding Key:

<img src="images/cognitive-3.png" width="600px;" style="padding:30px 3px 27px 0px;">

First, you should click on the relevant LINK, and jump to the relevant Microsoft website, at the bottom of the URL, there are several options to create the key, Note: Due to network condition it may take a long time to loading the page, please be patient, waiting for the page to load finished, finish page showing below:

<img src="images/cognitive-4.png" width="600px;" style="padding:30px 3px 27px 0px;">

As showing above, you can choose the “Try for free” button, which means to apply for a free Key of Bing Speech related, and then the page will relocation to the following page, showing as below:

<img src="images/cognitive-5.png" width="600px;" style="padding:30px 3px 27px 0px;">

From above, here you should click the “Create” button, and then the page will prompt the Microsoft voice recognition related Key application steps, showing as below:

<img src="images/cognitive-6.png" width="600px;" style="padding:30px 3px 27px 0px;">

From above, the two boxes are auto-checked, you just have to click the “Next” button to jump to the following page:

<img src="images/cognitive-7.png" width="600px;" style="padding:30px 3px 27px 0px;">

<img src="images/cognitive-8.png" width="600px;" style="padding:30px 3px 27px 0px;">

From above, select your account to log in to the Microsoft Azure system, follow to the boot steps, you will successfully get your Microsoft voice recognition related key, and then copy the corresponding content of the key to the corresponding box below , And then click the “SET” button to configure, showing as below:

<img src="images/cognitive-9.png" width="600px;" style="padding:30px 3px 27px 0px;">

After clicking the “SET” button, there will be three points of beating, which means the WiFi is configuring the Key of Bing Speech, please be patient to complete the configuration, showing as below:

<img src="images/cognitive-10.png" width="600px;" style="padding:30px 3px 27px 0px;">

When configuration process finished, there will be a correct check mark, means that Key configuration is done, if the configuration fails, it will prompt a break failure symbol, success configuration showing as below:

<img src="images/cognitive-11.png" width="600px;" style="padding:30px 3px 27px 0px;">

After the key is configured successfully, close the dialog, you will find that the Speech node has been configured Key successfully, in the text box, write the content that you want to express, and then click the play button, Microsoft Cognitive Services will convert your text into sound, and playe through the voice module.

<img src="images/cognitive-12.png" width="600px;" style="padding:30px 3px 27px 0px;">

Now please enjoy your time.

