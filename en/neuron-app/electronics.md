# Electronics Guide

In this section, you will find a list of electronics supported by Neuron App, and how to use them.

## Buttons and other Input Devices

**Button** outputs “yes” whenever you press it.

<img src="images/electronics-1.png" width="400px;" style="padding:30px 3px 3px 0px;">

>> <small><i>A Button is sending its output to a RGB LED</i></small>

**Knob** output its rotation position in a 0~100 number.

<img src="images/electronics-2.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>A Knob controlling the light strength of the RGB LED</i></small>

**Funny Touch** has 4 outputs, each outputting “yes-or-no” value. Please remember to attach the “Ground” wire.

<img src="images/electronics-3.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>A Funny Touch node working with a buzzer.<br>Touching each of the inputs produce a sound of a different pitch.</i></small>

**Joystick** has 2 outputs. X (-100~100) tells the left-or-right value and Y (-100~100) tells the up-or-down value.

<img src="images/electronics-4.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>An example of a Joystick controlling the Speed and Direction of a set of motors attached to a little car</i></small>

---

## Moving Parts

DC Motor has two motors. Inputting a number (0~100) makes them rotate in the same speed.

You may introduce a “Motor Speed” Companion Node to control each independent motors. It also receives a (0~100) number for each motor.

Another Companion Node, Steering, decides the speed and direction of a robot car. The car will move forward if “Speed” is positive and backward if negative. It will turn left if “Direction” is positive and left if negative.

<img src="images/electronics-5.png" width="400px;" style="padding:30px 3px 3px 0px;">

>> <small><i>DC Motor and its companion nodes</i></small>

Servo Node can attach two mini servos. Setting an angle (0~180 in degrees) turns the servo to a certain angle.

Servo Angle Companion Node controls servos in independent ports. Each port receives a 0~180 number value.
 
<img src="images/electronics-6.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Two Knobs controlling the angle of two Servos attached to the Servo Driver.</i></small>

---

## Showing Light, Sound and Data

RGB LED can light on in different colors. It receives a “yes-or-no” input to turn on / turn off the light. It also has a “Color” companion node allowing color switch under different conditions. The Color Companion Node also receives “yes-or-no” values.

<img src="images/electronics-7.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>When each virtual button is pressed, the RGB LED will show a different color</i></small>

LED Panel shows an image on the RGB LED light panel. You can select or draw an image on the iPad. Like RGB LED, it also has a Companion Node.

<img src="images/electronics-8.png" width="300px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Face panel and its Companion Node</i></small>

<img src="images/electronics-9.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Face Panel Image Editor</i></small>

LED Strip is a string of RGB LED lights. You can design patterns to display on the strip. The pattern may repeat itself or roll on the strip like a marquee.

<img src="images/electronics-10.png" width="300px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The LED Strip Node</i></small>

Display can display a number, an (English) sentence, or a robot face (emoticon). Companion Node “Text” and “Merge Text” can knit a string of text together. Companion Node “Face” tells the display which face to show when it receives “yes”.

<img src="images/electronics-11.jpg" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Display Node</i></small>

Buzzer can make a noise as a prompt or alarm. It buzz when the input receives “yes”. You may use its “Music Note” companion node to change the musical pitch of the sound.

<img src="images/electronics-12.jpg" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Buzzer Node</i></small>

---

## Sensors

Line Follower outputs “yes” when respected sensor head stays in a black line or dark surface.

Light Sensor outputs a number (0~100) indicating the strength of the light it perceived.

PIR sends out “yes” in front of human presence.

Humiture Sensor outputs two values: the air temperature (Degrees Celsius) and humidity (percentage).

<img src="images/electronics-13.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Sensors</i></small>

Temperature Sensor has a probe that measures temperature of liquids. It outputs number values in Degrees Celsius.

Sound Sensor outputs the volume of the environment sound. It outputs a 0~100 number.

Ultrasonic Sensor gives the distance between the sensor and an obstacle (in centimeter). Like a bat, it sends out a sound wave, wait for its return, and measure the travel time. If something blocks the emitter or the receiver, the sound wave will never return. This will result in a reading of largest distance: 300.

Color Sensor needs to work with its “Color Check” companion node. When the Color Sensor is close to a surface, the attached Color Check node can take the color of the surface. Then that Color Check node will output “yes” whenever the Color Sensor meets the same color. You can set the tolerance range of a Color Check via its config panel.

Soil Moisture Sensor gives a 0~100 number indicating the soil moisture.

<img src="images/electronics-14.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Sensors</i></small>

---

## Other Electronic Blocks

When Voice Recognition hears “Hello Makeblock”, it starts listening for specific commands. Then you can use its “Voice Command” companion node to test whether a specific command rings.

<img src="images/electronics-15.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Voice Recognition</i></small>

---