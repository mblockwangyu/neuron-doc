# Getting Started

Thank you for using Makeblock Neuron! Neuron is a set of durable electronic building blocks. It is easy to get started and flexible to use, making it excellent for learning and creating.

Neuron blocks have two modes: Online mode and Offline mode. When a chain of Neuron blocks is not connects to an iPad or a PC, it stays in the Offline Mode. In this mode, Neuron Blocks will behave according to the data it received from the previous block. For example, if a LED light follows a button, it will light up when someone presses the button.

<img src="images/get-started-1.png" width="400px;" style="padding:30px 3px 3px 0px;">

> > <small><i>An example of Neuron Blocks running under offline mode</i></small>

When a chain of Neuron block connects to an iPad or PC, either with Bluetooth or Wifi, it will act in the Online Mode. Now you can design how Neuron blocks interact with each other with the Neuron App in the iPad or PC. You may also introduce logic and math operations to explore more possibilities.

<img src="images/get-started-2.png" width="400px;" style="padding:30px 3px 3px 0px;">

> > <small><i>An example of Neuron Blocks running under online mode</i></small>

This document will introduce Makeblock Neuron App on iPad. And how it helps making creative work with Neuron Blocks.

---

## Connect with Bluetooth

The first thing to do is to connect Neuron Blocks to the iPad.

To connect with Bluetooth, you need a Bluetooth Block connected to the Power Block or an USB cable. After this, open the App and click the Bluetooth icon. Now you may hold your iPad and close up to the Bluetooth Block. If this does not work, tap “Pick from a List” button and tap the device name from the list. The name should start with “Neuron_” and followed by a unique number.

<img src="images/get-started-3.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Start connecting with Wifi or Bluetooth</i></small>

<img src="images/get-started-4.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Connect via Bluetooth</i></small> 

## Connect with Wifi

To connect with Wifi, you need a Wifi Block connected to the Power Block or USB Cable. Then click the Wifi icon and follow the prompt on the screen.

<img src="images/get-started-5.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Connect with Wifi Blocks</i></small>

## A tour of the Interface

<img src="images/get-started-6.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Interface of the Project Screen</i></small>

<img src="images/get-started-7.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>A tour of user interface</i></small>

## Using Neuron Electronic Blocks

When a Neuron Electronic Block adds to the chain, the block will pop up on the top of the screen. Then you may drag it down to the canvas.

The node often comes with outputs or inputs. You can draw a line between the output of a node and the input of another node to create connections. For example, if a button links to a LED light, it means the button “controls “the LED light. Pressing the button will turn on the light.

<img src="images/get-started-8.png" width="400px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The picture of a Neuron Node</i></small>

<img src="images/get-started-9.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Adding Nodes to the Canvas</i></small>

<img src="images/get-started-10.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>An example of a Button Node controlling an LED Node</i></small>

Behind the scene, Neuron App uses a “flow-based programming” model. This means the status of the button will be send to the LED Light each time it updates. The status of a node could be yes-or-no, numbers, or complex concepts like colors.
 
When you tap on some of the Neuron Block nodes, a Config Panel will popup. this will allow you change some properties like the color of the light, or the pattern of the light strip.
 
<img src="images/get-started-11.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using the Config Panel</i></small>

Some Neuron Blocks have “companion nodes”. For example, the LED Light node has a “color” companion node, which will appear in the bottom if you tap the LED Light node. You can drag the companion node up when it appears. You can use it to make LED lights show different colors under different conditions.

<img src="images/get-started-12.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using Companion Nodes</i></small>

<img src="images/get-started-13.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>An example of using “Color” Companion Node. LED shows different color<br>when the button is pressed or not.</i></small>

---

## Make Calculations

On the bottom of the screen, you will see a range of “Logic Nodes” organized in different tabs. They can bring your level of creation beyond simple “push a button and turn on the lights”. Like Neuron Block, Logic Nodes also have inputs, outputs, and sometimes config panels.

<img src="images/get-started-14.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Logic Nodes Panel</i></small>

Here is a list of some basic Logic Nodes:

**Compare Node** will output “yes” when the input meet the condition set on the config panel. Tap on the dropdown or the number to change the comparison condition.

<img src="images/get-started-15.png" width="300px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The “Compare” Node</i></small>

**Number Node** will output the number set on the config panel, if the input is “yes”. It is useful when setting the speed of a motor of the brightness of a light.

<img src="images/get-started-16.png" width="300px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Number Node</i></small>

<img src="images/get-started-17.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using the Number Node to control the light strength of a LED light</i></small>

**Compute Node** makes arithmetic operations like add, subtract, multiply, and divide.

<img src="images/get-started-18.png" width="300px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Compute Node</i></small>

**Interval Node** flips between “yes” and “no” at the interval set on the config panel. You can use it to make a LED blink or create a periodical operation.

<img src="images/get-started-19.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Make a blinking LED with the Interval Node</i></small>

**NOT, AND and OR nodes** runs boolean operation on every input and outputs yes-or-no value.

<img src="images/get-started-20.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using the AND Node. In this example, the light will be on only if both the button is pressed<br>and the knob reading is above 0</i></small>

**Toggle Node** flips between “yes” and “no” whenever the input changes from “no” to “yes”.

<img src="images/get-started-21.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using Toggle Node to turn a Button to a LED Switch</i></small>

**Counter Node** increase the number by 1 each time the input flips from “no” to “yes” and put the number through the output. It will reset the number to zero every time somebody presses the “reset” button, or the “reset” input receives a “yes”.

<img src="images/get-started-22.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Using the Counter Node. In this Example, the counter will count up as you press the button,<br>and the LED will turn brighter as the number runs up</i></small>

## Control Panels

In the “Control” tab of the bottom panel, you will find nodes like “Button” or “Label”. They are virtual control which you can interact in the iPad’s screen.

<img src="images/get-started-23.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>The Controls Node Panel</i></small>

<img src="images/get-started-24.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Click to open the Remote Control Screen</i></small>

Besides, on the top right corner, there is a Controller Button which will bring up a controller. It turns the iPad to a remote controller. Using it you can control your program at free without messing around with nodes and lines.

<img src="images/get-started-25.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>An example of the Remote Control Panel</i></small>

---

## Debugging

When things do not come up as expected, there are some basic tips to find the cause of the problem.

First, you can see a number or a symbol on the outputs of nodes. From this you can check the actual value it is outputting, like the readings of sensors or the result of logics.

<img src="images/get-started-26.png" width="600px;" style="padding:30px 3px 20px 0px;">

Second, the Button Node in the Control shelf is a useful tool for testing logic out. You can use virtual buttons to see results of logic graphs without using real hardware.

<img src="images/get-started-27.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>“Button” in the “Controls” panel is a good debugging tool</i></small>

Finally, as a general principle, testing ideas in smaller scales is always a good approach. If a big system goes wrong, try isolating the failed part by removing components. If you find the minimal case where the problem preserves, you are closer in finding the solution.

---

## Run on Wifi Blocks

Unlike the Bluetooth Block, the Wifi Block can run programs without iPads. After you finished linking nodes, click the “wifi” icon and “upload” to run programs in the Wifi Block. You can stop or update the program every time you connect to a Wifi Block.

<img src="images/get-started-28.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Click the Wifi icon to access the Wifi menu</i></small>

<img src="images/get-started-29.png" width="600px;" style="padding:30px 3px 3px 0px;">

>> <small><i>Upload your program to the Wifi Block</i></small>

---