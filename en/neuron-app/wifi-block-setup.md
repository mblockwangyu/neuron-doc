# Set up the Wi-Fi Block

In order to setup the Wifi Block, you need to visit the WLAN section in your iPad’s system settings, and connect to the hotspot with names starts with “neuron-“, as shown in the following picture:

<img src="images/wifi-1.png" width="600px;" style="padding:30px 3px 27px 0px;">

Then open the Neuron App and open the “Connect with Wifi” dialog with the Wifi icon:

<img src="images/wifi-2.png" width="600px;" style="padding:30px 3px 27px 0px;">


For first time users, click “Add a new Wifi Block”:

<img src="images/wifi-3.png" width="600px;" style="padding:30px 3px 27px 0px;">

Then you have two options: Click “Use in Offline Mode” to use the Wifi Block instantly, or **connect it to the Internet**. If the Wifi Block is not connected to the Internet, it will not be able to use the IoT functions.

---

## Connect the Wifi Block to the Internet

First, with Wifi Block connected, choose the Wifi hotspot of your home, school, or office. Then click “Next”.

<img src="images/wifi-4.png" width="600px;" style="padding:30px 3px 27px 0px;">

During the connection, the Wifi Block will try to restart its Wifi link. This may result in iPad disconnecting from the Wifi Block. In this case, you’ll need to reconnect to the Wifi Block’s Wifi hotspot (start with “neuron-“) as in the first step of **Setup the Wifi Block**.


**Here’s a way of checking if the Wifi Block’s Internet connection is successful:**

1. Using the iPad settings, connect to Wifi hotspot starts with “neuron-“;
2. Open your Internet Browser (eg. Safari) and see if you can open a Website. If the website shows up, it means the Wifi Block is connected to the Internet.

After this, tap the “Wifi” icon a second time, this screen will show up:

<img src="images/wifi-5.png" width="600px;" style="padding:30px 3px 27px 0px;">

Tap the Wifi Block icon in the dialog, and you are all set.