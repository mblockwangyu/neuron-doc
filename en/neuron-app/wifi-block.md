# Using the Wifi Block

The Wifi Block has much more functions than the Bluetooth Block. Besides the ability to run without the iPad as mentioned in “Run on Wifi Blocks”, it also connect to the Internet. It allows you control your creation in a distant place (see “Up on the Cloud” section). And it also provides two valuable attachments. All the attachments plug into the Wifi Block with an USB cable.

---

## Setup the Wifi Block

In order to setup the Wifi Block, you need to visit the WLAN section in your iPad’s system settings, and connect to the hotspot with names starts with “neuron-“, as shown in the following picture:

<img src="images/wifi-1.png" width="600px;" style="padding:30px 3px 27px 0px;">

Then open the Neuron App and open the “Connect with Wifi” dialog with the Wifi icon:

<img src="images/wifi-2.png" width="600px;" style="padding:30px 3px 27px 0px;">


For first time users, click “Add a new Wifi Block”:

<img src="images/wifi-3.png" width="600px;" style="padding:30px 3px 27px 0px;">

Then you have two options: Click “Use in Offline Mode” to use the Wifi Block instantly, or **connect it to the Internet**. If the Wifi Block is not connected to the Internet, it will not be able to use the IoT functions.

---

## Connect the Wifi Block to the Internet

First, with Wifi Block connected, choose the Wifi hotspot of your home, school, or office. Then click “Next”.

<img src="images/wifi-4.png" width="600px;" style="padding:30px 3px 27px 0px;">

During the connection, the Wifi Block will try to restart its Wifi link. This may result in iPad disconnecting from the Wifi Block. In this case, you’ll need to reconnect to the Wifi Block’s Wifi hotspot (start with “neuron-“) as in the first step of **Setup the Wifi Block**.


**Here’s a way of checking if the Wifi Block’s Internet connection is successful:**

1. Using the iPad settings, connect to Wifi hotspot starts with “neuron-“;
2. Open your Internet Browser (eg. Safari) and see if you can open a Website. If the website shows up, it means the Wifi Block is connected to the Internet.

After this, tap the “Wifi” icon a second time, this screen will show up:

<img src="images/wifi-5.png" width="600px;" style="padding:30px 3px 27px 0px;">

Tap the Wifi Block icon in the dialog, and you are all set.

---

## The Mic and Speaker Attachment

When using this attachment, a new “Sound” tab will appear on the nodes shelf. There you can use a series of sound related nodes.

Record node will start recording audio when inputed “yes” and will stop when inputed “no”. If the audio clip is too short, the app will discard the clip. So you need to ensure the input stays in “yes” for a period of time (for example, using “hold” node). It means that the button should be keep in pressed for several seconds.

You can pick up a file name in its Config panel to save it on the Wifi Block. Or you may link it to a “Voice to Text” Companion Node to convert the recording to text. Then you can use “Match Text” Companion Node to match text in the result. You need to setup Microsoft Cognitive Service when doing this.
 
<img src="images/wifi-6.png" width="600px;" style="padding:30px 3px 27px 0px;">

<img src="images/wifi-7.png" width="600px;" style="padding:30px 3px 27px 0px;">

<img src="images/wifi-8.png" width="600px;" style="padding:30px 3px 27px 0px;">

<small><i>You can record audio and convert it to text, and then use “Match Text” to determine whether the contents of the text match.</i></small>

Play Sound node can playback a sound effect (eg. cats’ meowing) or a prerecorded audio clip.

Speech Node will speak out the text inputted in the Config Panel through the speaker.

<img src="images/wifi-9.jpg" width="600px;" style="padding:30px 3px 27px 0px;">
 
<img src="images/wifi-10.png" width="600px;" style="padding:30px 3px 27px 0px;">
 
<small><i>“Play Sound” node can play effects and records</i></small>

Music Node can play a musical note when input turns “yes”. You can chain Music Nodes together to compose a melody.

<img src="images/wifi-11.png" width="600px;" style="padding:30px 3px 27px 0px;">

<small><i>You can drag “Play Music” from “Sound Tag”.</i></small>

---

## The Camera Attachment

With a camera connected, a “Camera” tab will popup on the Node Shelf. There you will find a camera icon in the bottom right corner, which opens a window showing the site of the camera.

Beside this, you can place a “Photo” node which takes photo when received “yes”. Then you can recognize the text in the photo (OCR), test whether people in the photo is smiling (Emotion Test). You may also send it to the Remote Control panel to view from remote location.

<img src="images/wifi-12.png" width="600px;" style="padding:30px 3px 27px 0px;">

---