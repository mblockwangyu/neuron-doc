# Gyro


![Node on the shelf](images/Gyro Sensor.png)
 
Have a sense of the angle and the movement.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Leveling Equipment 
Description:The gyroscope is used to sense the tilt of an object. You can learn how to level an object using the Neuron app.
Blocks Needed: Power, Bluetooth, LED Panel, Gyroscope, Neuron Board 


