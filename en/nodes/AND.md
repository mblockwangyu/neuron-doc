# And

![Node on the shelf](images/node-and-shelf.png)
 
YES if all inputs are YES.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：The alarm clock starts meowing like a cat at 15：10：00.

![Node on the canvas](images/node-and-canvas.png)

The logical “AND” node outputs “yes” only when all the input sources say “yes”. It can be used in situations like:

![Node example](images/node-and-example-1.png)

-	If the room temperature is high “AND” it is past 6 o’clock, turn on the fan

### How it works
 
![Node on the canvas](images/node-and-canvas.png)

The “AND” node says “yes” when all the sources of input are considered “yes”.

 
On a hot evening (temperature is over 30 and current hour is over 17, 5pm in the evening), turn on the fan (hooked up to a motor). 
