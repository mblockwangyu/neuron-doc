# Average

![AVERAGE Node on the shelf](images/node-average-shelf.png)
 
Average over a period of time.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/graph-average.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：The alarm clock starts meowing like a cat at 15：10：00.

### How it works

![AVERAGE Node on the canvas](images/node-average-canvas.png)

The result will be the average value of the input over a period of time. You can set the period of time (engineers call that sampling window) you want to perform the average in the config panel.

![Node example](images/node-average-example-1.png)
 
Example: a “baby monitor” that alarms the mother when the baby cries (which makes the gyro shaken for a period of time. The Y Acceleration is used to represent shaking). 

### Tips:
-	A smaller sampling window means the output is more sensitive to the change of the input.
