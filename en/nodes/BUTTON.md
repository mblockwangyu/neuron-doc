# Button

![Node on the shelf](images/Button.png)

Outputs YES if the button is pressed.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-button-knob-display.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


Case：A Safe
Description: Use the Knob block to make a cypher device. The password will be shown on the Display. If the password you input is correct, the indicator will turn on and you can open the safe by pressing the button.      
Blocks Needed：Power, Bluetooth, Button Knob, Display, DC Motor Driver, Servo Driver Pack, Magnet Wire 10cm.

