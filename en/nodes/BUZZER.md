# Buzzer

![Node on the shelf](images/Buzzer.png)

Makes a certain tone when input received.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-lightsensor-buzzer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Smart Pill Box
Description:The Light Sensor is used to sense the brightness of lights. And based on the signals given by the Light Sensor, the Buzzer sounds the alarm to remind people    
Blocks Needed: Power, Bluetooth, Light Sensor, Buzzer, Magnet Wire 10cm.