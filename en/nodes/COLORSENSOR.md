# Color Sensor

![Node on the shelf](images/Color Sensor.png)

Outputs Yes when certain color is detected.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case: Chameleon
Description: The Color Sensor recognizes color, the RGB LED displays color
Blocks Needed: Smart Power, Color Sensor, RGB LED