# Compare

![COMPARE Node on the shelf](images/node-compare-shelf.png)
 
Compare the inputting number with a pre-configured number.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/compare-sound.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock 
Function：The alarm clock starts meowing like a cat at 15:00:00 today. 

### How it works
 
![COMPARE Node on the Canvas](images/node-compare-canvas.png)

The input is the value to be compared. If the value satisfies the condition, the node will output “yes”; otherwise, it will output “no”.

![COMPARE Node example](images/node-compare-example-1.png)
 
When the temperature is over 30 (degrees Celsius), sound the buzzer.

### Tips: 

-	If the input is a string, the node will try to convert it to a number; if failed, the output will be “no”.
-	If the input is an object, the output will always be “no”