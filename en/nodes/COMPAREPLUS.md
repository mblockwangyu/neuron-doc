# Compare Plus

![COMPARE PLUS Node on the shelf](images/node-compareplus-shelf.png)
 
Compare the value of two input sources.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/random-compareplus.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case: small table lamp
Function: When the random number A>B, the light is on; when the random number A<B, the light is off


### How it works
 
![COMPARE PLUS Node on the canvas](images/node-compareplus-canvas.png)

Set the operator (>, <, =) in the config panel. Connect two operands to the input. The result will YES if the comparation condition is met. Otherwise it will output NO. 

![COMPARE PLUS Node example](images/node-compareplus-example-1.png)
 
Example: a light-sensor-based alarm device whose sensitivity is controlled by a knob
