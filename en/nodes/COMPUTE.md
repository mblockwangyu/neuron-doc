# Compute

![COMPUTE Node on the shelf](images/node-compute-shelf.png)
 
Make a certain arithmetic (+, -, *, /) operation on the input.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/compute-number.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels
Function：Display the multiples.

If you want your creation reacts to the slightest change in the room temperature, or you want the car moves, but have more juice when the color is blue, the “Compute” node comes to help. It can:
-	Amplify the input to a certain factor
-	Give an initial value of input by adding it to a constant number
-	Count numbers in a cycle by using the “mod” operator

### How it works 

![COMPUTE Node on the canvas](images/node-compute-canvas.png)

Pick an operator(“+”, “-“, “*”, “/”) and an operand (the number you want to add or subtract with). The output will be the input compute with the configured operand.

![Node example](images/node-compute-example-1.png)
 
Use the sound sensor value multiplied by 2 as the brightness of the RGB LED.

### Tips

-	It will output Error if the input is not a number
-	If you want to use the input as the right-hand side of the operator, use COMPUTE+ node in the “Advanced“ tab.
