# Compute Plus

![COMPUTE PLUS Node on the shelf](images/node-computeplus-shelf.png)
 
Make arithmetical operation on two inputs.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/computeplus.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels 
Function：Display the sum of the numbers. 

-	Make the result variates according to two input sources

### How it works

![COMPUTE PLUS Node on the canvas](images/node-computeplus-canvas.png)

Set the operator(+, -, *, /) in the config panel. The result will be <the first input> <operator> <the second input>. For example, if you connect A to the upper input, B to the lower input, and the operator is -, then the result will be A-B

