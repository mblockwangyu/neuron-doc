# Button

![COMPUTE Node on the shelf](images/Control-Button.png)

The node returns "Yes" when the button is pressed; it returns "No" when released.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：When the button is pressed, the light turns on; when the button is released, the light turns off.