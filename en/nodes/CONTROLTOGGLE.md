# Toggle

![COMPUTE Node on the shelf](images/Control-Toggle.png)

Make an on/off switch.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/toggle.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：When the button is pressed, the light turns on; when the button is released, the light turns off.