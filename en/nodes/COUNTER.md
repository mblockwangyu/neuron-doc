# Counter

![COUNTER Node on the shelf](images/node-counter-shelf.png)
 
Plus one when activated.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/counter.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Counter 
Function：Each time the button is pressed, the counter counts up once.

### How it works
 
![COUNTER Node on the canvas](images/node-counter-canvas.png)

When the input changes from “no” to “yes”(we call it a “rising edge” in electric engineering), the number displayed in the node will plus one, and goes to the output. 

If you push the “reset” button, the number will be reset to zero; the same will happen if the “RESET” input changes from “no” to “yes”.

![Node example](images/node-counter-example-1.png)
 
Example: count the length of time when the room filled with people (using the PIR “people sensor”). The result is expressed in seconds.

### Tips: 
-	If you want to make a countdown, use the COMPUTE+ node like this:

![Node example](images/node-counter-example-2.png)
 
Example: making a countdown
