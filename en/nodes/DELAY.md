# Delay

![DELAY Node on the shelf](images/node-delay-shelf.png)
 
Make it happen after some time.

### How it is used:

<video width="512" height="384" controls>
  <source src="../../node-videos/delay.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：5 seconds after the button is pressed, the light turns on; 5 seconds after the button is released, the light turns off. 

Delay node will take an input, and send it to the output after a set period of time. It is useful when:

-	You want something happens after an event but after a period of time
-	You want something happens after an event, and after a period of time, some other thing happens after an event.

### How it works:
 
Every input will be sent to the output after a certain period of time (provided in the config panel).
 
When the button is pressed, rotate the motor, then rotate it reversely after 1 second.

### Tip:

-	An easy way to understand the “DELAY” node is: the output is always certain seconds behind the input.
