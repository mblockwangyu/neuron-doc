# Gyro (Device)

![Node on the shelf](images/Devicegyro.png)

Collect data from the Gyro of your phone/tablet.

### How it is used:

<video width="512" height="384" controls>
  <source src="../../node-videos/device-gyro.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Animal Seesaw
Function：Tilt your phone in different directions to make your phone sound like different animals. 