# Sound Effect (Device)

![Node on the shelf](images/Devicesound.png)

Make a sound effect with your phone/tablet

### How it is used:

<video width="512" height="384" controls>
  <source src="../../node-videos/compare-sound.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：The alarm clock plays a Merry Christmas song at 15:00:00 today.