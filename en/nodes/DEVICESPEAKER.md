# Speaker (Device)

![Node on the shelf](images/Devicespeaker.png)

Play music with your phone/tablet

### How it is used:

<video width="512" height="384" controls>
  <source src="../../node-videos/device-speaker.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock 
Function：The alarm clock wakes me up by meowing at 15：00：00.

