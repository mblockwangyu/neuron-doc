# El Wires

![Node on the shelf](images/EL Wire Driver.png)

The EL wires can glow in the dark.

### How it is used:

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-elwires.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Energy-saving Lamp
Description: The lamp automatically turns on when it gets dark, illuminating the darkness for people; when it gets bright, the lamp automatically turns off for saving the energy.  
Blocks Needed: Power, Bluetooth, Light Sensor, EL Wire Driver, EL Wire, Magnet Wire 10cm.



