# Filter

![FILTER Node on the shelf](images/node-filter-shelf.png)
 
Only output the input if it falls within the certain range.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/filter.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：The lamp automatically lights up at 15:00:00 and turns off at 15:30:00.

### How it works

![FILTER Node on the canvas](images/node-filter-canvas.png)
 
Set a range in the config panel. If the input falls in the range, output the input itself; otherwise, output NO.

![FILTER Node example](images/node-filter-example-1.png)
 
Example: when the temperature is 0~20, turn green; when it is 20~30, turn yellow; when it is 30~40, turn red. 
