# Function

![FUNCTION Node on the shelf](images/node-function-shelf.png)
 
Pass the input through a mathematical function.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/function.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels 
Function：Display the magnifications. 

-	In case you needs to do some mathematical operation, like rounding a number to its nearest integer, FUNCTION comes to help.

### How it works
 
![FUNCTION Node on the canvas](images/node-function-canvas.png)
 
Set the function you want to use in the config panel. The output will always be the input passing through the function you choose.
 
The settings panel of the function

![FUNCTION Node example](images/node-function-example-1.png)
 
Example: a knob that can set an integer number from 1 to 10
