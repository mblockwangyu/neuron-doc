# Funny Touch

![Node on the shelf](images/Funny touch.png)

Reacts when you touch its 4 ends.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-funnytouch-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：The Ukulele
Description: You can touch different colored alligator clips to give the buzzer different sound effects.
Blocks Needed: Smart Power x 1, LED Panel x 1, Buzzer x 1, Funny Touch x 1, Alligator Clips x 1, GND Wire x 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-funnytouch-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>