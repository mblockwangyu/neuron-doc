# Hold
 
![HOLD Node on the shelf](images/node-hold-shelf.png)

Keep the input unchanged for a period of time

### How it is used

It can:

-	Keep the input value, until another not “no” value comes in 
-	Keep the input value unchanged for a period of time
-	Let the input change gradually

<video width="512" height="384" controls>
  <source src="../../node-videos/hold.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp 
Function：If the button is clicked, the light will stay on for 5 seconds and then will be turned off .

### How it works
 
![HOLD Node on the canvas](images/node-hold-canvas.png)

You can choose one of the three modes from the config panel: 

![Node config](images/node-hold-config-1.png)
 
Hold until change: the input will be kept until another input comes in (such as a sad face replaces a smiley face). In other words, the output could be any value other than “no”. 
 
![Node example](images/node-hold-example-1.png)

Example: change the color of the RGB LED with a FUNNY TOUCH (but do not turn off the light when untouched)

Hold for time: the input will stay the same for a period of time. In this period, any other input values will be ignored. If the hold period has been passed and the input is still “no”, the output will be set to “no”.

![Node example](images/node-hold-example-2.png)

An alarming device that beeps 3 seconds after light detected.

Change Slowly: the output will change according to the input, but if the input is a number, every second the change will be no more than the specified number.

![Node example](images/node-hold-example-3.png)

When the button is pressed, the light turns from green to red slowly (The number of the HOLD node’s config is set to 3).