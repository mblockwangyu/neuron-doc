# Humiture Sensor

![Node on the shelf](images/Humiture Sensor.png)

Tell the temperature and the humidity of the environment.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-humiture.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Automatic Clothes Collector
Description:The Servo block is used to control the angle of the two servos. The Humiture Sensor can sense the humidity of environments. Using the Neuron app, you can easily adjust the angle of the servo and make the two servos rotate by the same degrees. You also need to set a humidity threshold. Once the humidity exceeds the threshold, the clothes will be automatically taken back; when the humidity is lower than the threshold, the clothes will be automatically hanged outside for drying.
Blocks Needed: Power, Bluetooth, Humiture Sensor, Dual Servo Driver, Servo Accessory Pack, Magnet Wire 20cm, Friction Pin Connector 