# Indicator

![Node on the shelf](images/Indicator.png)

Tell if the input is Yes or not.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/toggle.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：When the button is pressed, the light turns on; when the button is released, the light turns off.