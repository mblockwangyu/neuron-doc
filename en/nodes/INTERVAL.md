# Interval
 
![INTERVAL Node on the shelf](images/node-interval-shelf.png)

Flips the output repeatedly, at a certain interval. 

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/repeat.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：A Flickering Lamp
Function：The lamp keeps flickering. 

The output of the interval node flips between “yes” and “no” by itself. It is useful when:

-	You want to make a blinking light or a jiggling robot
-	You want to do something periodically, like taking a photo every 30 seconds

### How it works
 
![INTERVAL Node on the canvas](images/node-interval-canvas.png)

In the config panel, choose the how many seconds before the result flips.

![Node example](images/node-interval-example-1.png)

Example: making a blinking light

![Node example](images/node-interval-example-2.png)

Example: making a “beep-beep” alarm when overheat