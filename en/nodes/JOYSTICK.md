# Joystick

![INTERVAL Node on the shelf](images/Joystick.png)

Move and control in two axis.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-joystick.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Rocker car
Description: Use the Motor block to drive the car and the Joystick block to control the Motor. Pair the car to the joystick with the Wireless Receiver block. Move the joystick all around to see how it make the car drive.
Blocks Needed: Power, Wireless Receiver, Joystick, DC Motor Driver, DC Motor Pack, Neuron Board, 