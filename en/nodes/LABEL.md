# Label

![INTERVAL Node on the shelf](images/Label.png)

Display a piece of data.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/random-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels
Function：Show us how the numbers change.