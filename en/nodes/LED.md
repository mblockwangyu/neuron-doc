# LED

![INTERVAL Node on the shelf](images/REG LED.png)

Emit light with a color

### How it is used

![NUMBER Node on the canvas](images/ex-RGB-LED.jpg)


Case：Rescuer
Description: When the device detects no people around, the LED light stays solid green. When it detects people in its proximity, the Buzzer will sound the alarm.  
Blocks Needed: Smart Power, IR Sensor, RGB LED, Buzzer