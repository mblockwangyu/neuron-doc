# Led Panel

![INTERVAL Node on the shelf](images/LED Panel.png)

Draw an image with light in a 8x8 LED matrix.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Infinity Band
Description: The Sound Sensor can sense the loudness and the LED Panel will react to the signals from the Sound Sensor. 
Blocks Needed: Smart Power x1, LED Panel x 1, Ranging Sensor x 1, Sound Sensor x 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>