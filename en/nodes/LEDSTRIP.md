# Led Strip

![INTERVAL Node on the shelf](images/LED Strip.png)

Make light with a strip of colorful LED

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Cloud Lamp 
Description: Whenever you come close to the lamp, the lamp will change its color. The Ranging Sensor is used to measure the distance and the LED Strip is responsible for changing the color of the lamp. 
Blocks Needed: Smart Power x1, LED strip x 1, Ranging Sensor x 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>