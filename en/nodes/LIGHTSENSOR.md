# Light Sensor

![INTERVAL Node on the shelf](images/Light Sensor.png)

Sense the strength of the light.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-lightsensor-buzzer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Smart Pill Box
Description: The Light Sensor block is used to sense the brightness. Based on the measurements, the sensor can check whether the pill box is opened or not; the buzzer can sound the alarm.  
Blocks Needed: Power, Bluetooth, Light Sensor, Buzzer, Magnet Wire 10cm.