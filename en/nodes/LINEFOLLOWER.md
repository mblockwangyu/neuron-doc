# Line Follower

![INTERVAL Node on the shelf](images/Line Follower.png)

Tell whether it is on a black line.

### How it is used

Case：Lamp 
Description: When an object comes close to the sensor on the left-hand side, the Light 1 turns on; when an object comes close to the sensor on the right-hand side, the Light 2 turns on.     
Blocks Needed: Smart Power, Dual IR Detector.