# Line Graph

![INTERVAL Node on the shelf](images/Line-Graph.png)

Show the progress of data across time.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/graph-average.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp 
Description: When an object comes close to the sensor on the left-hand side, the Light 1 turns on; when an object comes close to the sensor on the right-hand side, the Light 2 turns on.     
Blocks Needed: Smart Power, Dual IR Detector.