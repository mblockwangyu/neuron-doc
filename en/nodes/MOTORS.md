# Motors

![INTERVAL Node on the shelf](images/DC Motor Driver.png)

Drive two motors.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-motor-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：The Explorer
Description: The DC Motor Driver block can drive the two motors to rotate, which enables the car to drive as intended.
Blocks Needed: Smart Power x 1, LED Panel x 1, Buzzer x 1, DC Motor Driver x 1, LED Strip x 1, Ranging Sensor x 1, DC Motor x 2, LED Strip Driver x 1.