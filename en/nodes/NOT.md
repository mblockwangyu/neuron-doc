# Not

![Node on the shelf](images/node-not-shelf.png)
 
YES to NO and NO to YES.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/not.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：When the button is pressed, the lamp turns off; when the button is released, the lamp stays on.

Logical “NOT” stands for “if not”, “else” or “otherwise”. It flips the input from “yes” to “no” and “no” to “yes”. Useful in cases like:

-	Do something when the button is “NOT” pressed
-	If the Interval node is flipped to no, do something else

### How it works

![Node on the shelf](images/node-not-canvas.png)

Just attach the input, and get the opposite result.

![Node example](images/node-not-example-1.png)

Example: if the button is connected, turn red; otherwise, turn green

![Node example](images/node-not-example-2.png)
 
Color cycles between red and green
