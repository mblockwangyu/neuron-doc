# Now

![NOW Node on the shelf](images/node-now-shelf.png)
 
Get the hour, minute, and second now.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/or-compare.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：At 15：00：00 today, the alarm clock starts meowing like a cat. 

Node “NOW” outputs the current time, in the format of hour, minutes, and seconds. It can be used to:

-	Do something at a specific time of the day
-	Do something every second, every a few seconds or every minute.

### How it works
 
![NOW Node on the canvas](images/node-now-canvas.png)

There are 3 outputs in this node, and the meaning is straightforward: the hour(H), the minute(M), and the second(S) of the current time.

![NOW Node example](images/node-now-example-1.png)
 
Example: an alarm clock that buzzes for 10 seconds at 7:00
