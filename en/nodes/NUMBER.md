# Number
 
![NUMBER Node on the shelf](images/node-number-shelf.png)

Provide a number as a constant, or under a certain condition.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/number.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels
Function：Assign values to labels.

-	Set the parameter of another node. E.g., the rotation speed of a motor
-	Let another node’s parameter change to a certain value when something happens. E.g., when the heat is up, the fan motor rotates at a certain (higher) speed

### How it works
 
![NUMBER Node on the canvas](images/node-number-canvas.png)

The number set in the config panel will be sent to the output. 
If an input is provided, the value is sent only if the input is “yes”; otherwise the output will be “no”.

![NUMBER Node example](images/node-number-example-1.png)

Example: set the speed of the DC Motor Drive to 50

![NUMBER Node example](images/node-number-example-2.png)
 
Example: When the value of the light sensor is above 50, set the motor speed to 70. 
