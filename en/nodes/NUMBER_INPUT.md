# Number Input

![NUMBER Node on the shelf](images/node-number-shelf.png)

Input a number from the iPad

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/numbercontrol.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels
Function：Display the input numbers. 
