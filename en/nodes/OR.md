# Or

![OR Node on the shelf](images/node-or-shelf.png)
 
YES if any of the inputs is YES.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/or-compare.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock 
Function：The clock meows each time the minute hand points to fifteen. 

The logical “OR” node outputs “yes” if any of the input sources say “yes”. It can be used when:
-	If there is light (the drawer is open), sound the alarm; If the gyro is shaken (the creation is moved by somebody), also sound the alarm

### How it works

![OR Node on the canvas](images/node-or-canvas.png)
 
The “OR” node says “yes” whenever a source of the input is considered “yes”. 

![Node example](images/node-or-example-1.png)
 
Example: an alarming device that alarms when exposed to light (light sensor value > 30), or if it is moved (the gyro is shaken). 

Please note that the program above is equal to:

![Node example](images/node-or-example-1.png)

### Tips: 

-	In fact, you do not need an “OR” node very often because an “OR” logic is placed inside the input of most nodes. The example above is equal to this: 