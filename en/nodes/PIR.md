# Pir Sensor

![OR Node on the shelf](images/PIR Sensor.png)

Tell the presence of human beings

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-PIR.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lucky Cat
Description:Once someone is waving his or her hand, the PIR Sensor will sense the person and the servo driver will manipulate the cat to wave its hand.    
Blocks Needed: Smart Power, PIR Sensor, Dual Servo Driver, Servo.