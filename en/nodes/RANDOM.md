# Random

![RANDOM Node on the shelf](images/node-random-shelf.png)
 
Make an irregular output every time.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/random-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Rolling a Dice
Function：Set the number range to "1~6". Whenever the button is pressed, a random number ranging from 1 to 6 will be generated. 

The RANDOM node can generate a random number. It is used to:
-	Make a dice, or make an effect that produces a different result every time.

### How it works
 
When the input flips from NO to YES, a random number will be sent to the output. You can change the range of the random number in the config panel.

![RANDOM Node example](images/node-random-example-1.png)
 
Example: a light that changes its color randomly
