# Ranging Sensor

![RANDOM Node on the shelf](images/ranging sensor.png)

Measure the distance (range:2-200).

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-1.mp4" type="video/mp4">

Case：Cloud Lamp 
Description: Whenever you come close to the lamp, the lamp will change its color. The Ranging Sensor is used to measure the distance and the LED Strip is responsible for changing the color of the lamp. 
Blocks Needed: Smart Power x1, LED strip x 1, Ranging Sensor x 1.