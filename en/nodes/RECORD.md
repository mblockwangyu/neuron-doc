# Record

Record a piece of sound using the Wi-Fi Block.

**Record node** will start recording audio when inputed “yes” and will stop when inputed “no”. If the audio clip is too short, the app will discard the clip. So you need to ensure the input stays in “yes” for a period of time (for example, using “hold” node). It means that the button should be keep in pressed for several seconds.

<img src="../neuron-app/images/wifi-6.png" width="600px;" style="padding:30px 3px 27px 0px;">