# Scale
 
![SCALE Node on the shelf](images/node-scale-shelf.png)

Map the input from a range to another.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/scale.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Data Changes
Function：Compare the line chart including mapping with the one including no mapping. Show us how the values change.

-	Cases when the given input ranges from 0-100, but the output needs to be 0-255.
-	When the input changes too steadily or drastically, you can scale it to the range you want.

### How it works

![SCALE Node on the canvas](images/node-scale-canvas.png)
 
Set two ranges from the config panel. The input will be scaled according to the set range. 

![SCALE Node example](images/node-scale-example-1.png)
 
Example: make a thermometer. The temperature (0~50) is scaled to the angle of the servo (0~70) that has a needle stick on it that acts as a pointer.

### Tips:

-	You can use the Scale node to reverse a value. See the following example

![SCALE Node example](images/node-scale-example-2.png)

Example: make a light that changes according to the temperature. When it is hot, it turns red; when it is cold, it turns blue. The first SCALE node scales the range of temperature to 0~255, the range of light color. The second SCALE node makes the “blue” value changes to the opposite direction of the red value. 
