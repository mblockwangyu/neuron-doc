# Sequence

![SEQUENCE Node on the shelf](images/node-sequence-shelf.png)
 
Perform a series of actions according to their time.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/sequence.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：When the button is pressed, the alarm clock meows like a cat. After 5 seconds, the clock starts barking like a dog. 

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

The “SEQUENCE” node turns on each output one-by-one for a period of time. It can be used for:

-	Define an action as a series of moves. Such as shaking heads or waving hands
-	Perform a certain action when a certain condition is met

### How it works
  

Connect the action to the input of the SEQUENCE node; connect the moves to each of its outputs. Tap on the numbers on the node to set the duration of each output. And use the plus and minus sign to add/remove outputs.

![SEQUENCE Node example](images/node-sequence-example-1.png)

Example: a traffic light that greens for 10 seconds, yellows for 3 seconds, and red for 30 seconds.

![SEQUENCE Node example](images/node-sequence-example-2.png)
 
Example: when there is a person in front of the device (detected by the PIR sensor), nod head by repeatedly shaking the servo attached to port 1; otherwise, shake head by repeatedly shaking the servo attached to port 2.
