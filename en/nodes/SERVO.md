# Servo

![SEQUENCE Node on the shelf](images/Servo Driver.png)

Make an angle with servos.

### How it is used

 <video width="512" height="384" controls>
  <source src="../../node-videos/ex-servo.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Pulling up the Carrot
Description: The carrot keeps still when it is not pulled out. However, when it is pulled out, the carrot will show two "No" signs, as if it is saying "Don't touch me!" 
Blocks Needed: Power, Gyroscope, Dual Servo Driver, Servo Accessory Pack