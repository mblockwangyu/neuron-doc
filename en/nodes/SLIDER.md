# Slider

![SEQUENCE Node on the shelf](images/Slider.png)

Allow the user to change a number with a slider.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/slider-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels
Function：Show us how the numbers change.