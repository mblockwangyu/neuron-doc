# Soil Humidity Sensor

![SEQUENCE Node on the shelf](images/node-sequence-shelf.png)

Tell the humidity of the soil.

### How it is used

Case：Automatic Watering Device
Description:The Soil Sensor is used to sense the humidity changes of soil. If the humidity is too low, then the LED Panel will display a crying face, meaning the soil needs water; if the humidity is too high, then the LED Panel shows a smiley face, meaning it needs no more water.  
Blocks Needed: Smart Power, Soil Sensor, LED Panel.