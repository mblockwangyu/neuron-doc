# Sound Sensor

![SEQUENCE Node on the shelf](images/Sound Sensor.png)

Tell the loudness of sound (range: 1~100).

### How it is used

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Infinity Band
Description: The Sound Sensor block is used to sense the loudness of sounds. The LED Panel will react to the signals given by the Sound Sensor.  
Blocks Needed: Smart Power x 1, LED Panel x1 , Ranging Sensor x 1, Sound Sensor x 1.
