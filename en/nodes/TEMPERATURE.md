# Temperature

![SEQUENCE Node on the shelf](images/Temp Sensor.png)

Measure temperature of an object.

### How it is used

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-temp.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case: Thermometer
Description: Displays the degree by converting the temperature value detected by the temperature sensor into the angle value of the steering wheel rotation
Blocks Needed: Intelligent power supply, temperature sensor, dual servo drive, steering gear.