# Text Input

![SEQUENCE Node on the shelf](images/Text-Input.png)

Input a piece of text.

### How it is used

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/textbox-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Letter Labels
Function：Display the input letters. 