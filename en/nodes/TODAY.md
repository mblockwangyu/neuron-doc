# Today

![TODAY Node on the shelf](images/node-today-shelf.png)
 
Get the date (year, month, day) today.

### How it is used
<video width="512" height="384" controls>
  <source src="../../node-videos/devicespeaker.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Alarm Clock
Function：On December 25th, 2019, the clock plays a Merry Christmas song.

### How it works

![TODAY Node on the canvas](images/node-today-canvas.png)

There are 3 outputs in this node, and the meaning is straightforward: the day of the month(D), the month(M), and the day of the week(W) of today.

![TODAY Node example](images/node-today-example-1.png)
 
Example: light up the light strip on May the 4th
