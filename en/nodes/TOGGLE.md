# Toggle

![TOGGLE Node on the shelf](images/node-toggle-shelf.png)
 
Flip between YES and NO when activated.

### How it is used

<video width="512" height="384" controls>
  <source src="../../node-videos/toggle.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Lamp
Function：When the button is pressed, the light turns on; when the button is released, the light turns off.

![TOGGLE Node on the canvas](images/node-toggle-canvas.png)

The Toggle node flips between “yes” and “no”. You can use it to:
-	Convert a button to a switch. Push it, lights on (without the needs of keeping your finger on the button); push again, lights off.

### How it works
 
When the input changes from “no” to “yes”(we call it a “rising edge” in electric engineering), the output will flip between “yes” and “no”.

![Node example](images/node-toggle-example-1.png)
 
Example: use a button as a switch
