# Valve

![VALVE Node on the shelf](images/node-valve-shelf.png)

Output some value if the condition is met.

### How it is used


<video width="512" height="384" controls>
  <source src="../../node-videos/valve.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

Case：Number Labels 
Function：Display the number that is not filtered out by the valve. 

![VALVE Node on the canvas](images/node-valve-canvas.png)

-	“Conditional” nodes like COMPARE only outputs YES or NO; in case you want to output a value other than YES/NO, you will need the VALVE node.

### How it works
 
The upper input takes YES/NO; the lower input takes a value. If the upper input is YES, the lower input is sent to the node’s output; otherwise the node will output NO.
