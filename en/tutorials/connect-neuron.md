# Connect Neuron

You need a micro-USB cable and a Bluetooth block or the Wi-Fi Block to use Neuron with mBlock 5.

### Connect with the Bluetooth Block

1\. Use a USB cable to connect the Bluetooth Block to a USB port on your computer, as shown in the following picture:

<img src="images/usb-mblock.png" width="400px;" style="paddings:3px 3px 12px 0px;">

2\. Under "Devices", click "+" to add Neuron. Then click "Connect".

<img src="images/connect.gif" width="800px;" style="paddings:3px 3px 12px 0px;">
 