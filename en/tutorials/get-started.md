# Get Started

We will light up the LED Strip through programming.

We need these two Neuron blocks: <a href="./hardware/output/led-strip-driver.html" target="_blank">[LED Strip Driver]</a>, and <a href="./hardware/output/led-strip.html" target="_blank">[LED Strip]</a>.

1\. Connect [LED Strip Driver] and [LED Strip].

2\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when green flag clicked</span> to the Scripts Area. 

<img src="images/get-started-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. Add a Looks block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED strip (1) lights up ()</span>

<img src="images/get-started-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. Click the green flag and see what happens！

<img src="images/get-started-3.png" style="padding:3px 0px 15px 0px;">