<img src="../../zh/tutorials/images/neuron.jpg" style="width:600px;padding:3px 3px 16px 0px;">

# Introduction

The Neuron platform includes over 30 programmable electronic blocks, such as light sensor, infrared sensor, and more. The inclusive design not only appeals to children, but also open their mind. As children play with the blocks, they learn, and create.

### Colored Modules

All Neuron blocks fall into 3 main modules: Power & Communication, Input Blocks, and Output Blocks. Each block can be easily distinguished by its color.

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 1 &nbsp;</span> **Power & Communication**

Blocks used to power other blocks, or to establish wireless communication.

<img src="../../zh/tutorials/images/CommunicationModule.png" style="width:400px;padding:3px 3px 16px 0px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 2 &nbsp;</span> **Input Blocks**

Blocks used to collect data (such as light, or sound), and send the collected data to Output Blocks, so as to change output value.

<img src="../../zh/tutorials/images/InputModule.png" style="width:400px;padding:3px 3px 16px 0px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 3 &nbsp;</span> **Output Blocks**

Blocks used to receive and respond to signals sent by an input block, such as lighting up a LED, moving, making sound, etc.

<img src="../../zh/tutorials/images/OutputModule.png" style="width:400px;padding:3px 3px 16px 0px;">

---

### mBlock 5

Use mBlock 5 with Neuron and make more creations through programming. You can download mBlock 5 for PC, or just use mBlock 5 for Web browsers.

<img src="../../zh/tutorials/images/mblock5.png" style="width:200px;padding:3px 3px 16px 0px;">

- mBlock 5 PC: visit [http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/) to download
- mBlock 5 Web: [https://ide.mblock.cc/](https://ide.mblock.cc/)

<small>Note: mBlock 5 Web requires an extra software mLink to connect to your devices. Please refer to <a href="http://www.mblock.cc/doc/en/part-one-basics/mlink-quick-start-guide.html" target="_blank">mLink Guide</a>。</small>

---

### Neuron APP

The Neuron App is specially designed for Neuron. The flow-based programming is especially friendly. All you need to do is combine lines.

<img src="../../zh/tutorials/images/neuron.png" style="width:200px;padding:3px 3px 16px 0px;">

- Download: search "Neuron" on App Store or Google Play.

---

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;border-radius:5px;line-height:28px;">
<strong>Note:</strong><br>
<p>This help doc is mostly about using mBlock 5 with Neuron. The guide of Neuron App can be found in this chapter <a href="../neuron-app.html">[Neuron App]</a>.</p>
</div>
