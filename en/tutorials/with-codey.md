<img src="../../zh/tutorials/images/with-codey.jpg" style="padding:3px 0px 15px 0px;width:600px">

# Use Neuron with Codey

You can connect Neuron blocks to Codey and enjoy more fun.

Here is a simple project: light up the LED of Neuron LED Panel with Codey's Buttons.

1\. Connect the <a href="../hardware/output/led-panel.html" target="_blank">[LED Panel]</a> to Codey

2\. Connect Codey to mBlock 5. Under "Devices", choose "Codey", and click "Connect"

<img src="images/with-codey-0.png" style="padding:3px 0px 15px 0px;">

3\. Click "+ extension" at the bottom of the Blocks Area, and add "Neuron" blocks

<img src="images/with-codey-1.gif" style="padding:3px 0px 15px 0px;width:800px;">

4\. Drag an Events block <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">when button (A) is pressed</span> to the Scripts Area, and then add a Neuron block <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED panel lights up at x:(0) y:(0) with color (red)</span>

<img src="images/with-codey-2.gif" style="padding:3px 0px 15px 0px;width:800px;">

5\. Duplicate the script. Choose Button B and color "green"

<img src="images/with-codey-3.gif" style="padding:3px 0px 15px 0px;width:800px;">

6\. Upload the program

<img src="images/with-codey-4.gif" style="padding:3px 0px 15px 0px;width:800px;">

7\. Press the buttons of Codey and see what happens!