# Use the Wi-Fi Block

The Wi-Fi Block has much more functions than the Bluetooth Block. Besides the ability to run without the iPad as mentioned in “Run on Wifi Blocks”, it also connect to the Internet. It allows you control your creation in a distant place (see “Up on the Cloud” section). And it also provides two valuable attachments. All the attachments plug into the Wi-Fi Block with an USB cable.

* [Set up the Wi-Fi Block](neuron-app/wifi-block-setup.md)
* [The Mic and Speaker Attachment](wifi-mic-speaker.md)
    * [Record](nodes/RECORD.md)
    * [Play Sound](nodes/PLAYSOUND.md)
    * [Text to Speech](nodes/TEXTTOSPEECH.md)  
    * [Play Notes](nodes/PLAYNOTES.md)
* [The Camera Attachment](wifi-camera.md)
    * [Emotion Test](nodes/EMOTION_TEST.md)
    * [OCR](nodes/OCR.md)
    * [Photo Frame](nodes/PHOTO_FRAME.md)
    * [Snapshot](nodes/SNAPSHOT.md)