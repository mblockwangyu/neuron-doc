# The Camera Attachment

With a camera connected, a “Camera” tab will popup on the Node Shelf. There you will find a camera icon in the bottom right corner, which opens a window showing the site of the camera.

<img src="neuron-app/images/wifi-12.png" width="600px;" style="padding:30px 3px 27px 0px;">

* [Emotion Test](nodes/EMOTION_TEST.md)
* [OCR](nodes/OCR.md)
* [Photo Frame](nodes/PHOTO_FRAME.md)
* [Snapshot](nodes/SNAPSHOT.md)