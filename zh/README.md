# Makeblock 神经元

\* 如有任何技术疑问，请关注微信公众号：“Makeblock售后”

> 搜索微信号：`makeblock2013`  
> 或扫一扫  
> <img src="qr-code.jpg" width="100px;" style="padding:5px 5px 5px 0px;">

此帮助文档主要分为以下部分：

* [神经元](README.md)
  * [简介](tutorials/introduction.md)
  * [连接神经元](tutorials/connect-neuron.md)
  * [快速上手](tutorials/get-started.md)
  * [结合程小奔编程](tutorials/with-codey.md)
* [硬件说明](hardware/hardware.md)
  * [能源与通信](hardware/power-and-communication.md)
  * [输入模块](hardware/input.md)
  * [输出模块](hardware/output.md)
  * [附件](hardware/accessory.md)
* [案例](examples/examples.md)
  * [制作跳动的红心](examples/a-throbbing-red-heart.md)
  * [蜂鸣器奏乐](examples/play-music-with-buzzer.md)
  * [心随乐动](examples/musical-heartbeat.md)
  * [用震动控制LED面板-变脸](examples/face-changing-show.md)
  * [通讯变量-熊猫报数](examples/panda-counting-off.md)
  * [使用Funny Touch演奏乐器](examples/play-music-with-funny-touch.md)
* [神经元 App](neuron-app.md)
* [FAQ](faq/faq.md)

