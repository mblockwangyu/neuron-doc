# 控制节点

* [按钮](nodes/CONTROLBUTTON.md)
* [开关](nodes/CONTROLTOGGLE.md)
* [滑块](nodes/SLIDER.md)
* [指示灯](nodes/INDICATOR.md)
* [标签](nodes/LABEL.md)
* [折线图](nodes/LINE_GRAPH.md)
* [数字输入](nodes/NUMBER_INPUT.md)
* [文本输入](nodes/TEXT_INPUT.md)