# 制作跳动的红心

1\. 连接神经元「LED面板」模块。

<small>注：模块介绍请参考<a href="../hardware/output/led-panel.html" target="_blank">「LED面板」</a>。</small>

2\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区。

<img src="images/throbbing-heart-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. 添加4个外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED面板（1）显示图案（）持续（1）秒</span>，实现红心跳动效果。

<img src="images/throbbing-heart-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 点击绿色旗帜运行程序。

<img src="images/throbbing-heart-3.jpg" style="padding:3px 0px 15px 0px;">

4\. 你也可以绘制更多图案，比如星星或雪花。

<img src="images/throbbing-heart-4.jpg" style="padding:3px 0px 15px 0px;">

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>小技巧：</strong><br>
&#9755; 在积木上点击右键可以复制脚本<br>
<img src="images/throbbing-heart-5.jpg" style="padding:3px 0px 15px 10px;width:300px;"><br>
&#9755; 在脚本区的空白处点击右键，可以整理所有积木
<img src="images/throbbing-heart-6.jpg" style="padding:3px 0px 15px 10px;width:300px;">
</div>

5\. 保存你的程序。