# 案例

### 入门

* [制作跳动的红心](a-throbbing-red-heart.md)
* [蜂鸣器奏乐](play-music-with-buzzer.md)
* [心随乐动](musical-heartbeat.md)

### 中级

* [用震动控制LED面板-变脸](face-changing-show.md)
* [通讯变量-熊猫报数](panda-counting-off.md)
* [使用Funny Touch演奏乐器](play-music-with-funny-touch.md)

<a href="案例.rar" download>点击下载代码</a>

---

### 更多资源

<img src="../../en/examples/images/education.png" style="padding: 0px 3px 3px 0px;width:150px;">

您可以登录 makeblock 官方教育站，获得更多教育资源：<br>[http://education.makeblock.com/zh-hans/resource/](http://education.makeblock.com/zh-hans/resource/)

