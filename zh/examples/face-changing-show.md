# 用震动控制LED面板 - 变脸

该项目使用了侦测类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">陀螺仪传感器（1）感受到摇晃？</span>

<img src="images/face-changing-1.png" style="padding:3px 0px 15px 0px;">

为了使得陀螺仪能够控制LED面板上的图案，我们需要使用控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>，当判断条件为真时，语句块内的语句块会被执行，否则它将被跳过。

<img src="images/face-changing-2.png" style="padding:3px 0px 15px 0px;">

需要使用<a href="../hardware/output/led-panel.html" target="_blank">「LED面板」</a>、和<a href="../hardware/input/gyro.html" target="_blank">「陀螺仪」</a>三个模块。

1\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>，侦测类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">陀螺仪传感器（1）感受到摇晃？</span>，还有外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED面板（1）显示图案（）持续（1）秒</span>，并修改显示图案

<img src="images/face-changing-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. 复制脚本，并分别修改LED面板显示的图案

<img src="images/face-changing-4.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/face-changing-5.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. 点击绿色旗帜运行程序，摇晃陀螺仪，LED面板上的表情就会随之变化