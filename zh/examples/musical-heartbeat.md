# 心随乐动

将积木组合起来就形成了脚本。

<small>注：最多可以同时执行六个脚本。</small>

该程序包含了两个脚本，一个脚本控制LED面板绘制跳动的红心，一个脚本控制蜂鸣器鸣唱歌曲。

需要使用<a href="./hardware/output/buzzer.html" target="_blank">「蜂鸣器」</a>和<a href="./hardware/output/led-panel.html" target="_blank">「LED面板」</a>两个模块。

### 脚本一：蜂鸣器鸣唱歌曲

1\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加四个声音类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">蜂鸣器（1）播放音符（）以（0.25）拍</span>，分别选择音符：C4、D4、E4、C4

<img src="images/music-buzzer-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. 继续完善这组音乐，组成以下程序，注意使用复制技巧

<img src="images/music-buzzer-3.jpg" style="padding:3px 0px 15px 0px; width:360px;">

3\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/musical-heartbeat-1.gif" style="padding:3px 0px 15px 0px; width:800px;">


### 脚本二：跳动的红心

4\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，并添加两个外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED面板（1）显示图案（）持续（1）秒</span>

<img src="images/musical-heartbeat-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

5\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/musical-heartbeat-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

6\. 点击绿色旗帜运行程序

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>小技巧：</strong><br>
&#9755; 使用 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span> 语句块可以使得程序被重复的执行
</div>
