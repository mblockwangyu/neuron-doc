# 通讯变量-熊猫报数

该项目使用了侦测类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">陀螺仪传感器（1）感受到摇晃？</span>

<img src="images/face-changing-1.png" style="padding:3px 0px 15px 0px;">

当陀螺仪检测到震动时，（摇晃或拍击桌面均能使得放在桌上的陀螺仪检测到震动）该输出结果为 True，否则为 False。

在本项目中我们将使用变量来使得小熊猫感知陀螺仪的震动次数。需要使用<a href="./hardware/input/gyro.html" target="_blank">「陀螺仪」</a>模块。

### 为陀螺仪编程

1\. 选择变量类积木，点击“新建变量”，并将新变量命名为“震动次数”

<img src="images/panda-counting-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">将（震动次数）设为（0）</span>

<img src="images/panda-counting-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>，侦测类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">陀螺仪传感器（1）感受到摇晃？</span>，还有变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">将（震动次数）增加（1）</span>

<img src="images/panda-counting-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/panda-counting-4.gif" style="padding:3px 0px 15px 0px; width:800px;">

### 为 Panda 编程

5\. 选中“角色”下的“Panda”

<small>注：你也可以选择其它角色。</small>

<img src="images/panda-counting-0.png" style="padding:3px 0px 15px 0px;">

6\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">说（）</span>，并将变量类积木 <span style="background-color:#FF8C1A;color:white;padding:3px; font-size: 12px">震动次数</span> 放入

<img src="images/panda-counting-5.gif" style="padding:3px 0px 15px 0px; width:800px;">

7\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/panda-counting-6.gif" style="padding:3px 0px 15px 0px; width:800px;">

8\. 点击绿色旗帜运行程序，敲击桌面查看程序运行效果。

<img src="images/panda-counting-1.png" style="padding:3px 0px 15px 0px;width：400px;">

你可以和小伙伴们比赛，看在固定时间内谁的震动次数大。
