# 蜂鸣器奏乐

这个项目将会告诉你如何使用神经元的蜂鸣器发声，并制作一段小音乐。

1\. 连接神经元「蜂鸣器」模块。

<small>注：模块介绍请参考<a href="../hardware/output/buzzer.html" target="_blank">「蜂鸣器」</a>。</small>

2\. 该项目使用了声音类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">蜂鸣器（1）播放音符（）以（0.25）拍</span>

<img src="images/music-buzzer-2.jpg" style="padding:3px 0px 15px 0px; width:360px;">

3\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加四个声音类积木 <span style="background-color:#CF63CF;color:white;padding:3px; font-size: 12px">蜂鸣器（1）播放音符（）以（0.25）拍</span>，分别选择音符：C4、D4、E4、C4

<img src="images/music-buzzer-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

4\. 继续完善这组音乐，组成以下程序，注意使用复制技巧

<img src="images/music-buzzer-3.jpg" style="padding:3px 0px 15px 0px; width:360px;">

5\. 点击绿旗尝试播放这段音乐，你听出来这是哪一首曲子了吗？

<div style="background-color:#E7F1F9;padding:20px;border-top:5px solid #6EACDB;font-size:14px;line-height:28px;margin-left:10px;margin-bottom:15px;border-radius:5px;width:450px;"><strong>小技巧：</strong><br>
&#9755; 可以通过改变节拍来调整音乐播放的速度。
</div>