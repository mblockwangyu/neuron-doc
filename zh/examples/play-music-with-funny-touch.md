# 使用 Funny Touch 演奏乐器

使用广播的功能可以让神经元与舞台角色通讯，完成舞台互动作品:

<img src="images/funny-touch-0.png" style="padding:3px 0px 15px 0px;">

需要使用<a href="./hardware/input/touch.html" target="_blank">「触摸开关 Funny Touch」</a>模块。

### 为 Funny Touch 编程

1\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区，再添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">如果（）那么（）</span>，侦测类积木 <span style="background-color:#4CBFE6;color:white;padding:3px; font-size: 12px">触摸开关（1）（蓝色）被触摸？</span>，还有事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">广播（）</span>，并新建消息“Blue”。让蓝色鳄鱼夹被触摸时发送广播消息“Blue”。

<img src="images/funny-touch-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

2\. 通过程序复制，同理新建“Yellow”，“Red”，“Green”三个广播消息，并选择对应颜色的鳄鱼夹。

<img src="images/funny-touch-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 添加控制类积木 <span style="background-color:#FFAB19;color:white;padding:3px; font-size: 12px">重复执行</span>

<img src="images/funny-touch-3.gif" style="padding:3px 0px 15px 0px; width:800px;">

### 为舞台角色编程

4\. 点击“角色”，删除默认角色“Panda”

<img src="images/funny-touch-4.gif" style="padding:3px 0px 15px 0px; width:800px;">

5\. 点击“+”添加角色“Max”。

<img src="images/funny-touch-5.gif" style="padding:3px 0px 15px 0px; width:800px;">

6\. 在积木区，点击最下方的“+添加扩展”，添加“音乐”扩展。

<img src="images/funny-touch-6.gif" style="padding:3px 0px 15px 0px; width:800px;">

7\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当接收到（Blue）</span> 拖到脚本区，并添加音乐类积木 <span style="background-color:#0FBD8C;color:white;padding:3px; font-size: 12px">弹奏鼓声（(1)小军鼓）（0.25）拍</span>

<img src="images/funny-touch-7.gif" style="padding:3px 0px 15px 0px; width:800px;">

8\. 复制程序，当接收到“Yellow”、“Red”、“Green”消息时，分别弹奏鼓声“(2)低音鼓”、“(3)鼓边敲击”、“(4)碎音钵”。

<img src="images/funny-touch-8.gif" style="padding:3px 0px 15px 0px; width:800px;">

9\. 点击绿色旗帜，看一下效果吧！

<img src="images/funny-touch-00.png" style="padding:3px 0px 15px 0px;">