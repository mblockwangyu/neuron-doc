<img src="images/neuron-all.jpg" style="padding:3px 3px 15px 0px;">

# 神经元FAQ

## 1.我购买过你们的智造家套件，全部的模块都可以和程小奔结合么？

神经元智造家套件包含以下模块：

·**四触摸模块**

**·LED面板**

**·蜂鸣器**

**·舵机**

**·陀螺仪**

目前智造家套件的LED面板和蜂鸣器可以和程小奔结合，之后的慧编程版本会逐渐增加对神经元模块的支持。

---

## 2.我购买过你们的创新实验室套件（Creative Lab），全部模块都可以和程小奔结合使用么？

目前创新实验室套件（Creative Lab）的以下模块可以和程小奔结合使用，之后的慧编程版本会逐渐增加对神经元模块的支持：

·**LED面板**

**·灯带**

**·蜂鸣器**

**·超声波传感器**

**·人体红外传感器**

**·土壤湿度传感器**

---

## 3.我还没有买过神经元，可以买单个模块和程小奔结合吗？

是的，国内用户可以在天猫上购买神经元单个模块：[makeblock 天猫旗舰店](https://makeblock.tmall.com/category-1371338295.htm?spm=a220o.1000855.0.0.1275507fv57WJk&search=y&parentCatId=1338296503&parentCatName=%C9%F1%BE%AD%D4%AA&catName=%C1%E3%BC%FE)

---

FAQ会持续更新~

