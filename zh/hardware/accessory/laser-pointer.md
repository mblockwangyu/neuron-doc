<img src="images/laser-pointer-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/laser-pointer-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/laser-pointer-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/laser-pointer-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 激光发射模块

激光发射模块能够发射指向性极其高的光束，配合光线传感器能检测是否有物体经过。可用它实现有趣的案例创意。

- 净重量：10.8g
- 体积：24×24×27mm

### 参数

- 激光发射功率：1mW
- 电池容量：85mAh   
- 续航：≥10小时
- 充电时间：≈100分钟
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 可充电
- 超长续航
- 定制化激光模组，不易伤眼"
