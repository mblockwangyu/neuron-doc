<img src="images/magnet-wire-10cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/magnet-wire-20cm-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/magnet-wire-10cm-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/magnet-wire-20cm-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">


# 磁吸连接线

磁性连接线能够连接神经元模块，有10cm、20cm两种规格。

**10cm 磁吸连接线**

- 净重量：10.3g
- 体积：100×24×12mm

**20cm 磁吸连接线**

- 净重量：12.1g
- 体积：200×24×12mm

### 参数

- 连接寿命：＞6000次插拔

### 特点

- 安全亲肤材质
- 精致外形
