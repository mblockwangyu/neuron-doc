<img src="images/neuron-board-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/neuron-board-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/neuron-board-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/neuron-board-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 磁吸板包

包含6个磁吸板与36个摩擦销。	

- 净重量：93.3
- 体积：48×48×8.3mm

### 参数

- 材质：ABS

### 特点

- 兼容乐高
- 兼容 Makeblock 金属零件平台
- 支持磁性吸附
- 多种组合方式，支持创意搭建
					
