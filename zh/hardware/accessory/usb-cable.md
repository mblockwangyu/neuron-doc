<img src="images/usb-cable-20cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/usb-cable-100cm-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">


# USB 数据线

USB数据线提供数据传输和充电功能，有20cm、100cm两种规格。

**20cm USB数据线**

- 净重量：9.6g
- 体积：20cm

**100cm USB数据线**

- 净重量：25.6g
- 体积：100cm

### 参数

- 连接寿命：＞3000次插拔

### 特点

支持数据传输
