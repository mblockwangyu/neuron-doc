# 硬件说明

### 能源与通信

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="power-and-communication/power.html" target="_blank"><img src="power-and-communication/images/power.jpg" width="250px;"></a><br>
<p>电源模块</p></td>

<td width="33%;"><a href="power-and-communication/bluetooth.html" target="_blank"><img src="power-and-communication/images/bluetooth.jpg" width="250px;"></a><br>
<p>蓝牙模块</p></td>

<td width="33%;"><a href="power-and-communication/wireless.html" target="_blank"><img src="power-and-communication/images/wireless.jpg" width="250px;"></a><br>
<p>无线收发模块</p></td>
</tr>

<tr>
<td><a href="power-and-communication/wifi.html" target="_blank"><img src="power-and-communication/images/wifi.jpg" width="250px;"></a><br>
<p>Wi-Fi模块</p></td>
</tr>
</table>

### 输入模块

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="input/joystick.html" target="_blank"><img src="input/images/joystick.jpg" width="250px;"></a><br>
<p>摇杆</p></td>

<td width="33%;"><a href="input/button.html" target="_blank"><img src="input/images/button.jpg" width="250px;"></a><br>
<p>按钮</p></td>

<td width="33%;"><a href="input/knob.html" target="_blank"><img src="input/images/knob.jpg" width="250px;"></a><br>
<p>旋钮</p></td>
</tr>

<tr>
<td><a href="input/touch.html" target="_blank"><img src="input/images/touch.jpg" width="250px;"></a><br>
<p>触摸开关</p></td>
<td><a href="input/light-sensor.html" target="_blank"><img src="input/images/light-sensor.jpg" width="250px;"></a><br>
<p>光线传感器</p></td>
<td><a href="input/dual-ir.html" target="_blank"><img src="input/images/dual-ir.jpg" width="250px;"></a><br>
<p>双路红外开关</p></td>
</tr>

<tr>
<td><a href="input/ultrasonic.html" target="_blank"><img src="input/images/ultrasonic.jpg" width="250px;"></a><br>
<p>超声波模块</p></td>
<td><a href="input/sound-sensor.html" target="_blank"><img src="input/images/sound-sensor.jpg" width="250px;"></a><br>
<p>声音传感器</p></td>
<td><a href="input/pir-sensor.html" target="_blank"><img src="input/images/pir-sensor.jpg" width="250px;"></a><br>
<p>人体红外传感器</p></td>
</tr>

<tr>
<td><a href="input/camera.html" target="_blank"><img src="input/images/camera.jpg" width="250px;"></a><br>
<p>摄像头</p></td>
<td><a href="input/color-sensor.html" target="_blank"><img src="input/images/color-sensor.jpg" width="250px;"></a><br>
<p>色彩识别</p></td>
<td><a href="input/gyro.html" target="_blank"><img src="input/images/gyro.jpg" width="250px;"></a><br>
<p>陀螺仪</p></td>
</tr>

<tr>
<td><a href="input/temperature-sensor.html" target="_blank"><img src="input/images/temperature-sensor.jpg" width="250px;"></a><br>
<p>温度传感器</p></td>
<td><a href="input/humiture-sensor.html" target="_blank"><img src="input/images/humiture-sensor.jpg" width="250px;"></a><br>
<p>温湿度传感器</p></td>
<td><a href="input/soil-moisture-sensor.html" target="_blank"><img src="input/images/soil-moisture.jpg" width="250px;"></a><br>
<p>土壤湿度传感器</p></td>
</tr>

</table>

### 输出模块

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="output/rgb-led.html" target="_blank"><img src="output/images/led.jpg" width="250px;"></a><br>
<p>RGB灯</p></td>

<td width="33%;"><a href="output/el-wire-driver.html" target="_blank"><img src="output/images/el-wire-driver.jpg" width="250px;"></a><br>
<p>冷光线驱动</p></td>

<td width="33%;"><a href="output/el-wire-1.html" target="_blank"><img src="output/images/el-wire-a-0.jpg" width="250px;"></a><br>
<p>冷光管（红绿黄橙）</p></td>
</tr>

<tr>
<td><a href="output/el-wire-2.html" target="_blank"><img src="output/images/el-wire-b-0.jpg" width="250px;"></a><br>
<p>冷光管（蓝粉紫白）</p></td>
<td><a href="output/buzzer.html" target="_blank"><img src="output/images/buzzer.jpg" width="250px;"></a><br>
<p>蜂鸣器</p></td>
<td><a href="output/dual-dc-motor.html" target="_blank"><img src="output/images/dual-dc-motor-driver.jpg" width="250px;"></a><br>
<p>双电机驱动</p></td>
</tr>

<tr>
<td><a href="output/dc-motor.html" target="_blank"><img src="output/images/dc-motor.jpg" width="250px;"></a><br>
<p>直流电机</p></td>
<td><a href="output/water-pump.html" target="_blank"><img src="output/images/water-pump.jpg" width="250px;"></a><br>
<p>水泵</p></td>
<td><a href="output/dual-servo-driver.html" target="_blank"><img src="output/images/dual-servo-driver.jpg" width="250px;"></a><br>
<p>双舵机驱动</p></td>
</tr>

<tr>
<td><a href="output/servo.html" target="_blank"><img src="output/images/servo.jpg" width="250px;"></a><br>
<p>小舵机组件</p></td>
<td><a href="output/led-panel.html" target="_blank"><img src="output/images/led-panel.jpg" width="250px;"></a><br>
<p>LED面板</p></td>
<td><a href="output/led-strip-driver.html" target="_blank"><img src="output/images/led-strip-driver.jpg" width="250px;"></a><br>
<p>灯带驱动</p></td>
</tr>

<tr>
<td><a href="output/led-strip.html" target="_blank"><img src="output/images/led-strip.jpg" width="250px;"></a><br>
<p>灯带</p></td>
<td><a href="output/display.html" target="_blank"><img src="output/images/display.jpg" width="250px;"></a><br>
<p>显示屏</p></td>
<td><a href="output/speaker.html" target="_blank"><img src="output/images/speaker.jpg" width="250px;"></a><br>
<p>扬声器</p></td>
</tr>
</table>

### 附件

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="accessory/laser-pointer.html" target="_blank"><img src="accessory/images/laser-pointer.jpg" width="250px;"></a><br>
<p>激光发射模块</p></td>

<td width="33%;"><a href="accessory/neuron-board.html" target="_blank"><img src="accessory/images/neuron-board.jpg" width="250px;"></a><br>
<p>磁吸板包</p></td>

<td width="33%;"><a href="accessory/magnet-wire.html" target="_blank"><img src="accessory/images/magnet-wire.jpg" width="250px;"></a><br>
<p>磁吸连接线</p></td>
</tr>

<tr>
<td><a href="accessory/usb-cable.html" target="_blank"><img src="accessory/images/usb-cable.jpg" width="250px;"></a><br>
<p>USB数据线</p></td>
<td><a href="accessory/clip-and-rubber-band.html" target="_blank"><img src="accessory/images/clip-rubber-band-2.jpg" width="250px;"></a><br>
<p>固线夹橡皮筋混合包</p></td>
</tr>
</table>