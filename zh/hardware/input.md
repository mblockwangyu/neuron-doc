# 输入模块

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="input/joystick.html" target="_blank"><img src="input/images/joystick.jpg" width="250px;"></a><br>
<p>摇杆</p></td>

<td width="33%;"><a href="input/button.html" target="_blank"><img src="input/images/button.jpg" width="250px;"></a><br>
<p>按钮</p></td>

<td width="33%;"><a href="input/knob.html" target="_blank"><img src="input/images/knob.jpg" width="250px;"></a><br>
<p>旋钮</p></td>
</tr>

<tr>
<td><a href="input/touch.html" target="_blank"><img src="input/images/touch.jpg" width="250px;"></a><br>
<p>触摸开关</p></td>
<td><a href="input/light-sensor.html" target="_blank"><img src="input/images/light-sensor.jpg" width="250px;"></a><br>
<p>光线传感器</p></td>
<td><a href="input/dual-ir.html" target="_blank"><img src="input/images/dual-ir.jpg" width="250px;"></a><br>
<p>双路红外开关</p></td>
</tr>

<tr>
<td><a href="input/ultrasonic.html" target="_blank"><img src="input/images/ultrasonic.jpg" width="250px;"></a><br>
<p>超声波模块</p></td>
<td><a href="input/sound-sensor.html" target="_blank"><img src="input/images/sound-sensor.jpg" width="250px;"></a><br>
<p>声音传感器</p></td>
<td><a href="input/pir-sensor.html" target="_blank"><img src="input/images/pir-sensor.jpg" width="250px;"></a><br>
<p>人体红外传感器</p></td>
</tr>

<tr>
<td><a href="input/camera.html" target="_blank"><img src="input/images/camera.jpg" width="250px;"></a><br>
<p>摄像头</p></td>
<td><a href="input/color-sensor.html" target="_blank"><img src="input/images/color-sensor.jpg" width="250px;"></a><br>
<p>色彩识别</p></td>
<td><a href="input/gyro.html" target="_blank"><img src="input/images/gyro.jpg" width="250px;"></a><br>
<p>陀螺仪</p></td>
</tr>

<tr>
<td><a href="input/temperature-sensor.html" target="_blank"><img src="input/images/temperature-sensor.jpg" width="250px;"></a><br>
<p>温度传感器</p></td>
<td><a href="input/humiture-sensor.html" target="_blank"><img src="input/images/humiture-sensor.jpg" width="250px;"></a><br>
<p>温湿度传感器</p></td>
<td><a href="input/soil-moisture-sensor.html" target="_blank"><img src="input/images/soil-moisture.jpg" width="250px;"></a><br>
<p>土壤湿度传感器</p></td>
</tr>

</table>