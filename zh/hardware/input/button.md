<img src="images/button-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/button-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/button-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/button-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 按钮

按钮是一种常见的触发设备，可以作为开关去开启和关闭你的发明。

- 净重量：7.7g
- 体积：24×24×21mm

### 参数

- 使用寿命：＞10000次
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

长按（5s）保持触发功能。
