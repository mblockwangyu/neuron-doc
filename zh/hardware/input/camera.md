<img src="images/camera-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/camera-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/camera-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/camera-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 摄像头

摄像头用于采集图像和影像信息，通过模块间的组合完成各种创意活动。	

- 净重量：8.1g
- 体积：24×24×22mm

### 参数

- 分辨率：1280×720   
- 像素大小：3.4um×3.4um
- 最大图像传输速率：全尺寸 @ 30fps
- 视场角：100°
- 镜片结构：4G+IR   
- 焦比：2.97
- 有效焦距：2.4mm
- 功耗：100uA(待机)~240mW(工作)
- 电源：USB总线电源    
- 抗跌落能力：1m
- 工作温度：-30℃~70℃（可以使用）、0℃~50℃（稳定图像）
- 工作湿度：＜95%"

### 特点

- 高清分辨率
- 支持AI，具有人脸识别，文字识别功能
- 配合WiFi模块支持离线运行
