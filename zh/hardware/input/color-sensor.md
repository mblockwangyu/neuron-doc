<img src="images/color-sensor-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/color-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/color-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/color-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 色彩识别模块

色彩识别模块用于识别物体的颜色，并会返回一组RGB数值。

- 净重量：7.3g
- 体积：24×24×14mm

### 参数

- 刷新率：25Hz
- 动态范围：3800000:1 
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 高灵敏度
- 配合软件支持取色功能

