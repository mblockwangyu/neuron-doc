<img src="images/dual-ir-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/dual-ir-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dual-ir-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dual-ir-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 双路红外开关

双路红外开关由两对红外光反射式光电开关组成，可以作为两个独立的按钮或用于自动巡线。

- 净重量：11.2g
- 体积：24×48×14mm

### 参数

- 测量范围：0~2cm
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"	

### 注意事项

神经元独占（指在模块化电子积木中，该模块只在神经元中拥有）
