<img src="images/gyro-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/gyro-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/gyro-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/gyro-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 陀螺仪

陀螺仪模块可以检测物品的运动状态和倾斜角度，提供当前的角度和加速度信息；也可用于检测震动。

- 净重量：7.4g
- 体积：24×24×14mm

### 参数

- 俯仰角范围：±180°
- 翻滚角范围：±90°
- 加速度范围：±8g 
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 支持姿态识别
- 可检测多种不同强度的震动及晃动
