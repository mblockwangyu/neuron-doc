<img src="images/humiture-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/humiture-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/humiture-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/humiture-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 温湿度传感器

温湿度传感器用于检测当前环境的温湿度值。

- 净重量：7.5g
- 体积：24×24×14mm

### 参数

- 温度值量程：-40℃~125℃
- 温度值精度：±1℃
- 湿度值量程：0%~100%
- 湿度值精度：±3%
- 工作电压：DC 5V
- 抗跌落能力：1.5m

### 特点

- 多功能，可同时检测环境的温度与湿度。

### 注意事项

模块不防水，请勿直接与水接触
