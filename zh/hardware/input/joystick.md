<img src="images/joystick-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/joystick-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/joystick-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/joystick-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 摇杆

摇杆能够带来非常直观且有趣的操作体验，使用它控制你的发明前后左右移动。	

- 净重量：10.0g
- 体积：24x24x25mm

### 参数

- 使用寿命：＞10000次
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"	

### 特点

进口摇杆模组
