<img src="images/knob-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/knob-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/knob-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/knob-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 旋钮

旋钮可以调节数值输入的大小，完成对亮度、声音频率或速度大小的控制。

- 净重量：8.2g
- 体积：24×24×22.5mm

### 参数

- 使用寿命：＞10000次
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

金属旋钮组件，大幅度提高耐用性。
