<img src="images/light-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/light-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/light-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/light-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 光线传感器

光线传感器用于检测周围环境中的光线强弱，它接受到的光线越强，相应的输出信号就越强。

- 净重量：7.3g
- 体积：24×24×14mm

### 参数

- 测量范围：0~100（相对值）
- 测量精度：±5
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"
