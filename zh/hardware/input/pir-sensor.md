<img src="images/pir-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/pir-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/pir-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/pir-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 人体红外传感器

人体红外传感器可以检测3米内人的运动。如果有人在范围内运动，红外识别传感器就会被触发。

- 净重量：8.7g
- 体积：24x24x25.6mm

### 参数

- 测量范围：0~3m
- 感应角度：120°
- 初始化时间：5s以内
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"
