<img src="images/soil-moisture-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/soil-moisture-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/soil-moisture-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/soil-moisture-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 土壤湿度传感器

土壤湿度传感器模块用于检测土壤湿度。

- 净重量：17.2g
- 体积：24×24×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度（模块）：＜95%

### 注意事项

土壤具备一定的腐蚀能力，请勿长期将土壤湿度传感器探头放在土壤中。
						
