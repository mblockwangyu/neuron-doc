<img src="images/sound-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/sound-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/sound-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/sound-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 声音传感器

声音传感器用于检测周围环境中的声音强度，它接收的声音强度越大，输出的信号越强。

- 净重量：7.5g
- 体积：24×24×14mm

### 参数

- 测量范围：0~100（相对值）
- 测量精度：±5
- 灵敏度：-38±3dB
- 方向性：全向
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"


