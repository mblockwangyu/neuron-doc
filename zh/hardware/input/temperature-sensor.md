<img src="images/temperature-sensor-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/temperature-sensor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/temperature-sensor-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/temperature-sensor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 温度传感器

温度传感器模块用于检测水、人体等物体的温度。

- 净重量：13.7g
- 体积：24×24×14mm

### 参数

- 工作电压：5V
- 测量范围：-55℃~125℃
- 测量精度：±0.5℃（-10℃~85℃）
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度（模块）：-10℃~55℃
- 工作湿度：＜95%"	

### 特点

- 高灵敏度	

### 注意事项

测量温度超过100℃时, 传感器线材可能会损坏。请小心使用以免受损坏设备。
