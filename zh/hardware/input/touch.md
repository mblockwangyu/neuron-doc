<img src="images/touch-2.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/touch-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/touch-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/touch-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">


# 触摸开关


触摸开关可以连接导电的物品，将它变成触摸开关。通过检测四色鳄鱼夹和地线的导通状态，能实现简单有趣的人机交互。	

- 净重量：28.8g
- 体积：24×48×14mm

### 参数

- 触发电阻范围：＜24MΩ 
- 夹子大小：35mm
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%	

### 特点

16种的输入组合
与其他模块丰富的离线互动效果
多样化的触发方式
定制化平口鳄鱼夹，大幅度减小安全隐患
					
					
