<img src="images/ultrasonic-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/ultrasonic-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/ultrasonic-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/ultrasonic-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 超声波模块

超声波模块组件用于检测与前方障碍物的距离，检测范围为3~300cm。	

- 净重量：12.4g
- 体积：24×48×14mm

### 参数

- 测量范围：3~300cm
- 测量精度：±5%
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

神经元独占
