# 输出模块

<table  cellpadding="15px;" cellspacing="12px;" style="font-size: 12px;text-align:center;">
<tr>
<td width="33%;"><a href="output/rgb-led.html" target="_blank"><img src="output/images/led.jpg" width="250px;"></a><br>
<p>RGB灯</p></td>

<td width="33%;"><a href="output/el-wire-driver.html" target="_blank"><img src="output/images/el-wire-driver.jpg" width="250px;"></a><br>
<p>冷光线驱动</p></td>

<td width="33%;"><a href="output/el-wire-1.html" target="_blank"><img src="output/images/el-wire-a-0.jpg" width="250px;"></a><br>
<p>冷光管（红绿黄橙）</p></td>
</tr>

<tr>
<td><a href="output/el-wire-2.html" target="_blank"><img src="output/images/el-wire-b-0.jpg" width="250px;"></a><br>
<p>冷光管（蓝粉紫白）</p></td>
<td><a href="output/buzzer.html" target="_blank"><img src="output/images/buzzer.jpg" width="250px;"></a><br>
<p>蜂鸣器</p></td>
<td><a href="output/dual-dc-motor.html" target="_blank"><img src="output/images/dual-dc-motor-driver.jpg" width="250px;"></a><br>
<p>双电机驱动</p></td>
</tr>

<tr>
<td><a href="output/dc-motor.html" target="_blank"><img src="output/images/dc-motor.jpg" width="250px;"></a><br>
<p>直流电机</p></td>
<td><a href="output/water-pump.html" target="_blank"><img src="output/images/water-pump.jpg" width="250px;"></a><br>
<p>水泵</p></td>
<td><a href="output/dual-servo-driver.html" target="_blank"><img src="output/images/dual-servo-driver.jpg" width="250px;"></a><br>
<p>双舵机驱动</p></td>
</tr>

<tr>
<td><a href="output/servo.html" target="_blank"><img src="output/images/servo.jpg" width="250px;"></a><br>
<p>小舵机组件</p></td>
<td><a href="output/led-panel.html" target="_blank"><img src="output/images/led-panel.jpg" width="250px;"></a><br>
<p>LED面板</p></td>
<td><a href="output/led-strip-driver.html" target="_blank"><img src="output/images/led-strip-driver.jpg" width="250px;"></a><br>
<p>灯带驱动</p></td>
</tr>

<tr>
<td><a href="output/led-strip.html" target="_blank"><img src="output/images/led-strip.jpg" width="250px;"></a><br>
<p>灯带</p></td>
<td><a href="output/display.html" target="_blank"><img src="output/images/display.jpg" width="250px;"></a><br>
<p>显示屏</p></td>
<td><a href="output/speaker.html" target="_blank"><img src="output/images/speaker.jpg" width="250px;"></a><br>
<p>扬声器</p></td>
</tr>
</table>