<img src="images/buzzer-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/buzzer-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/buzzer-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/buzzer-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 蜂鸣器模块

蜂鸣器会根据接收到的信号或编程指令发出不同的声音，可用于模拟音符演奏简单的音乐，如模拟报警器、门铃、电话等音源发声。

- 净重量：7.9g
- 体积：24×24×15mm

### 参数

- 频率范围：0~20000Hz
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"
