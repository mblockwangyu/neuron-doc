<img src="images/dc-motor-5.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/dc-motor-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dc-motor-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dc-motor-5.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 直流电机

电机需配合双电机驱动使用。它可以旋转，还可以通过编程控制转速和旋转方向。可用于制作小车、机械臂、弹射装置等。

- 净重量：49.9g

### 参数

- 额定转速：118±10%RPM
- 齿轮比：1:100
- 额定转矩：132g/cm 
- 停转转矩：590±100g/cm 
- 额定电流：0.1A    
- 堵转电流：0.4A     

### 特点

含乐高十字转接件及 Makeblock 金属平台标准轮，兼容乐高和 Makeblock 金属零件平台。
