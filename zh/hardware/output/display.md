<img src="images/display-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/display-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/display-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/display-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 显示屏

显示屏模块能够显示输入值。你也可以通过编程自定义要显示的信息，如数字、标点和表情信息等。

- 净重量：12.4g
- 体积：24×48×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

支持图像、表情、ASCII编码字符等多种输出。
