<img src="images/dual-servo-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/dual-servo-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dual-servo-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/dual-servo-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 双舵机驱动

双舵机驱动模块可以同时驱动两个舵机转动。

- 净重量：12g	
- 体积：24×48×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

可以同时驱动两个舵机运行
