<img src="images/ei-wire-a-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

<img src="images/ei-wire-a-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">


# 冷光管 A （红绿黄橙）

需配合冷光线驱动模块使用，提供红绿黄橙四种颜色的冷光线。

- 净重量：32.5g
- 体积：1000mm

### 参数

- 发光强度：100nits
- 外层材质：PVC
- 抗拉力：＜1kg
- 使用寿命：≥5000小时
- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 注意事项

冷光线驱动在工作时会发出轻微噪音,属正常现象，不影响使用。
