<img src="images/ei-wire-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/ei-wire-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/ei-wire-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/ei-wire-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 冷光驱动模块

冷光线驱动模块可以同时驱动4种不同颜色的冷光线发光，结合铁丝和连杆能够做出别具一格的创意。

- 净重量：17.2g
- 体积：24×48×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

可同时驱动四路冷光线
