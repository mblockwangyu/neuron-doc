<img src="images/led-panel-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/led-panel-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-panel-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-panel-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# LED 面板

LED面板由8×8的RGB灯点阵组成，能够输出表情、字母、动画等。

- 净重量：20.1g
- 体积：48×48×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 强大色彩表现力
- 支持滚动英文字母及数字
