<img src="images/led-strip-driver-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/led-strip-driver-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-strip-driver-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-strip-driver-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 灯带驱动

灯带驱动模块用于驱动灯带发亮，实现多种创意效果。

- 净重量：7.5g
- 体积：24×48×14mm

### 参数

- 工作电压： DC 5V
- 抗跌落能力： 1.5m
- 工作温度：-10℃~55℃
- 工作湿度： ＜95%

### 特点

预设多种灯光效果
