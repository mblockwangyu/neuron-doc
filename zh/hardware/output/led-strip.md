<img src="images/led-strip-0.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/led-strip-1.jpg" width="250px;" style="padding:3px 5px 12px 0px;">
<img src="images/led-strip-2.jpg" width="250px;" style="padding:3px 0px 12px 0px;">


# 灯带（0.5m）

需配合灯带驱动模块使用。灯带由15颗RGB灯组成，每颗RGB灯都可以被独立的编程，发出指定的颜色。

- 净重量：20g
- 体积：500mm

### 特点

拥有15颗独立可编程RGB灯。


