<img src="images/led-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/led-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/led-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# RGB 灯

RGB灯模块会根据接收到的信号或编程指令发出不同的灯光颜色和亮度，可快速制作出红绿灯、手电筒、小夜灯等精彩案例。

- 净重量：7.3g
- 体积：24×24×14mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

丰富色彩表现
