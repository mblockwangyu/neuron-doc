<img src="images/servo-4.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/servo-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/servo-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/servo-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 舵机

舵机需配合双舵机驱动使用，可在0~180°内来回转动。通过编程，舵机能转动至指定角度，完成特定动作。

- 净重量：24.5

### 参数

- 极限角度：180°±10°
- 齿轮虚位：≤1° 
- 停转转矩（锁定状态）：1.5±0.05kg·cm 
- 待机电流（停止状态）：6±1mA
- 工作电压：4.8V~6.0V 
- 工作温度：-10℃-50℃
- 存储温度：-20℃~60℃

### 特点

丰富配件，兼容乐高和 Makeblock 金属零件平台。
