<img src="images/speaker-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/speaker-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/speaker-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/speaker-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 扬声器

扬声器模块能够录制并播放声音。

- 净重量：24.5g
- 体积：48×48×13mm

### 参数

- 工作电压：DC 5V
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 支持录音存储
- 内置多种音效
- 配合软件支持AI语音识别
- 配合WiFi模块支持离线运行"
