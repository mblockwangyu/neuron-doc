<img src="images/water-pump-3.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/water-pump-0.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/water-pump-1.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/water-pump-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 水泵

水泵组件能够将水抽到高处，用于实现水泵案例创意。

- 净重量：91.2
- 体积：D27×75mm

### 参数

- 水孔直径：6.5mm
- 噪声：＜60dB
- 额定电压：12V   
- 电流(带负载)：＜320mA  
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

工作噪音低
