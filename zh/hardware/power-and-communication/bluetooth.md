<img src="images/bluetooth-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/bluetooth-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/bluetooth-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/bluetooth-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 蓝牙模块

蓝牙模块用于建立模块与平板电脑、手机或makeblock蓝牙适配器之间的连接，也可通过连接移动电源或电脑等为其他模块供电。

- 净重量：11.3g	
- 体积：24×48×14mm

### 参数

- 蓝牙版本：BT4.0
- 频带范围：2402~2480MHz
- 天线增益：1.5dBi
- 能耗等级：≤4dBm
- FCC ID：2AH9Q-NU001BT
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%

### 特点

- 支持串口通信
- 支持为其他模块供电
