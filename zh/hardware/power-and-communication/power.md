<img src="images/power-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/power-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/power-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/power-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# 电源模块

电源模块能够为各模块供电。连接好模块后，轻按开启电源。长按3秒即可关闭电源。

- 净重量：39.4g
- Volume: 48×48×14mm


### 参数

- 电池容量：950mAh 
- 续航时间: 最长可达4小时
- 充电时长：≈75分钟
- 输出电压：DC 5V 
- 输入电压：DC 5V     
- 输入电流：＜1A 
- 抗跌落能力：1.5m
- 工作温度：0℃~45℃ 
- 储存温度：-10℃~55℃ 
- 工作湿度：＜95%
- 使用寿命：循环次数≥300

### 特点

- 实时电量显示
- 与模块断开后自动关机，延长续航
- 具备充电保护保护功能

### 注意事项

- 闲置时请至少三个月充一次电
- 在充电时不能同时为其他模块供电
- 续航时间为理想测试环境，受连接模块数目影响较大，仅做参考
- 电池容量在在反复充电后存在一定下降，系锂电池特性决定，300次充电后电池容量不低于初始值的80%