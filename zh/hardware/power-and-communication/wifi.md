<img src="images/wifi-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/wifi-2.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/wifi-3.jpg" width="200px;" style="padding:3px 0px 12px 0px;">
<img src="images/wifi-4.jpg" width="200px;" style="padding:3px 0px 12px 0px;">

# Wi-Fi 模块

Wi-Fi模块能够在平板电脑、手机和模块间建立无线Wi-Fi连接，可结合云服务实现IoT功能。	

- 净重量：25.7g
- 体积：48×48×14mm	

### 参数

- 无线标准：IEEE 802.11b/g/n（HT20）
- 频带范围：2412~2462MHz
- 工作模式：STA/AP/STA+AP
- 通信柜离：10m（空旷环境下）
- 工作电压：DC 5V    
- 工作电流：200mA
- FCC ID：2AH9Q-NU001WF
- 抗跌落能力：1.5m
- 工作温度：0℃~45℃
- 工作湿度：＜95%

### 特点

- 支持离线运行程序
- 支持配置到互联网进行IoT应用
- 支持通过 WiFi 模块与APP直接连接

### 注意事项

- 请保证 Wi-Fi 模块具有良好的供电环境
- WiFi 模块的启动，需要一定时间（30~60秒），请耐心等待
