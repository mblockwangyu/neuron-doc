<img src="images/wireless-1.jpg" width="450px;" style="padding:3px 0px 12px 0px;">

<img src="images/wireless-2.jpg" width="300px;" style="padding:3px 0px 12px 0px;">
<img src="images/wireless-3.jpg" width="300px;" style="padding:3px 0px 12px 0px;">


# 无线收发模块

无线收发模块能提供快速稳定的无线通讯功能。装有无线发送模块的神经元串能够给装有无线接收模块的神经元串发送信号并控制它。

- 净重量：22.4g
- Volume: 24×48×14mm

### 参数

- 蓝牙版本：BT4.0
- 频带范围：2402~2480MHz
- 天线增益：1.5dBi
- 通信距离：10m
- 能耗等级：≤4dBm
- FCC ID：2AH9Q-NU001WT
- 抗跌落能力：1.5m
- 工作温度：-10℃~55℃
- 工作湿度：＜95%"

### 特点

支持通过物理连接进行快速配对	

### 注意事项

配对需要一定时间，配对完成后将无线收发模块重新与其他模块组合后，连接仍需要一定时间，请耐心等待。（10s以内）

