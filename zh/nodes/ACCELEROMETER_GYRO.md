# 陀螺仪

![Node on the shelf](images/Gyro Sensor.png)

检测物体震动和倾斜方向。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-gyro.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：水平仪

- 说明：利用陀螺仪传感器检测物体的倾斜，通过神经元APP编程指示出该如何调整使得物体水平。

- 所需模块：电源、蓝牙模块、LED面板、陀螺仪传感器、磁吸板。

### 工作原理
 
![Node on the canvas](images/node-and-canvas.png)

当所有的输入都是Yes的时候，与节点才输出Yes。