# 与

![Node on the shelf](images/node-and-shelf.png)
 
连接的多个输入都相当于Yes的时候才会输出Yes。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


-  案例：小闹钟

-  功能：今天15：10：00，响起猫叫声小闹钟

### 工作原理
 
![Node on the canvas](images/node-and-canvas.png)

当所有的输入都是Yes的时候，与节点才输出Yes。
