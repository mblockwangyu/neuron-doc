# 平均

![AVERAGE Node on the shelf](images/node-average-shelf.png)
 
取一定时间内的平均值

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/graph-average.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：数据变化趋势

- 功能：加入平均值和不加平均值的折线图形成对比，显示数字改变的规律

### 工作原理

![AVERAGE Node on the canvas](images/node-average-canvas.png)

结果将会是一段时间段输入的平均值。你可以设置配置项中的时间段长度（工程师叫它采样窗） 


### 提示

-	短的采样窗意为着输出对输入变化的反应更敏感。
