#蜂鸣器

![Node on the shelf](images/Buzzer.png)

播放音符

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-lightsensor-buzzer.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：智能药盒

- 说明：光线传感器可以检测光线强弱，以此来确认药盒是否打开；蜂鸣器可以发出声音，进行提醒。

- 所需模块：电源、蓝牙模块、光线传感器、蜂鸣器、连接线10cm。