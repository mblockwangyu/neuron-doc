#颜色传感器

![Node on the shelf](images/Color Sensor.png)
播放音符

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：变色龙

- 颜色传感器识别颜色，LED灯珠发出颜色

- 所需模块：智能电源、颜色传感器