# 比较

![COMPARE Node on the shelf](images/node-compare-shelf.png)
 
比较输入的数值与原来的数值。 

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小闹钟

- 功能：今天15：00：00后，响起猫叫声小闹钟

### 工作原理
 
![COMPARE Node on the Canvas](images/node-compare-canvas.png)

输入的值将用来被比较。如果这个值满足条件，那么这个节点将会输出“yes”；否则，输出“no”。 

### 提示: 

-	如果输入的是一个字符串，节点会尝试将它转化为数字；如果失败，将会输出“no”

-	如果输入的是一个对象，输出将始终是“no”