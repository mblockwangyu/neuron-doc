# 比较+

![COMPARE PLUS Node on the shelf](images/node-compareplus-shelf.png)
 
比较两个输入的大小

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/random-compareplus.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小台灯

- 功能：当随机数A>B时，灯亮；当随机数A<B时灯灭

### 工作原理
 
![COMPARE PLUS Node on the canvas](images/node-compareplus-canvas.png)

在配置面板中设置操作符(>，<，=)，将两个数值连接到节点的输入端。如果满足比较条件，结果将输出YES。否则将输出NO。
