# 计算

![COMPUTE Node on the shelf](images/node-compute-shelf.png)

对输入数值进行加、减、乘、除等四则运算

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/compute-number.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数字标签

- 功能：显示数字的倍数

### 工作原理 

![COMPUTE Node on the canvas](images/node-compute-canvas.png)

选择一个操作符（“+”, “-“, “*”, “/”）和一个操作数（你想添加或删减的数字）。
输出的就是配置项中输入的计算结果。 


### Tips

-	如果输入不是一个数字，它将会输出错误 

-	如果你想运算符在输入的右边，请用“高级”菜单里的 “计算+”节点 
