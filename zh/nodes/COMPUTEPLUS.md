# 计算+

![COMPUTE Node on the shelf](images/node-compute-shelf.png)

对两个输入进行加、减、乘、除四则运算

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/computeplus.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数字标签

- 功能：显示数字的和

### 工作原理 

![COMPUTE Node on the canvas](images/node-compute-canvas.png)

选择一个操作符（“+”, “-“, “*”, “/”）

输出的就是两个输入的比较结果。 


