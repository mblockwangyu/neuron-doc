# 计算+

![COMPUTE Node on the shelf](images/Control-Button.png)

按下时输出Yes，松开时输出No

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/and-compare-time.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小台灯

- 功能：按下按钮灯亮，松开灯灭

