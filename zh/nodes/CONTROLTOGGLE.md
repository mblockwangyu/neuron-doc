# 开关

![COMPUTE Node on the shelf](images/Control-Toggle.png)

这是一个开关。通过点击操作让开关在“开”与“关”状态之间来回切换。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/toggle.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小台灯

- 功能：按下按钮灯常亮，松开灯关闭