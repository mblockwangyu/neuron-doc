# 计数器 

![COUNTER Node on the shelf](images/node-counter-shelf.png)

每触发一次，数字增加1，并将数字提供给输出

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/counter.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：计数器

- 功能：每按下一次按钮，计一次数

### 工作原理
 
![COUNTER Node on the canvas](images/node-counter-canvas.png)

当输入从“no”转化为“yes”时（在电子工程中我们叫它“上升沿”），节点中的数字显示将会加1，并输出。

如果你按下“重置”按钮，数字将会被重置为0；如果“reset”的输入端口由“no”变为“yes”，也会发生同样的事。


### 提示 
-	如果你想做一个倒计时，用”计算+“节点，像这样： 

![Node example](images/node-counter-example-2.png)
 
例子：制作倒计时