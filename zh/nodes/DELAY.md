# 延时

![DELAY Node on the shelf](images/node-delay-shelf.png)
 
让事件延迟发生

### 用法:

<video width="512" height="384" controls>
  <source src="../../node-videos/delay.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小台灯

- 功能：按下按钮，5s后灯亮，松开按钮，5s后灯灭

### 工作原理:
 
每一个输入都会在一段时间后（配置项中提供）被传送到输出。 
当按钮被按下后，转动电机；一秒后再反转

