# 陀螺仪

![Node on the shelf](images/Devicegyro.png)
 
判断手机或平板的倾斜状态

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/device-gyro.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


-  通过向不同方向倾斜手机，发出不同的动物叫声

### 工作原理
 
![Node on the canvas](images/node-and-canvas.png)

当所有的输入都是Yes的时候，与节点才输出Yes。