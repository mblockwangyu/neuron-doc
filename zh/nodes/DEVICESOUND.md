# 音效

![Node on the shelf](images/Devicesound.png)

产生动物叫声等音效

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/compare-sound.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小闹钟

- 功能：今天15：00：00后，播放圣诞歌