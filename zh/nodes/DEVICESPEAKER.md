# 扬声器

![Node on the shelf](images/Devicespeaker.png)

播放音符或者歌曲

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/device-speaker.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小闹钟

- 功能：今天15：00：00后，用猫叫声叫醒我
