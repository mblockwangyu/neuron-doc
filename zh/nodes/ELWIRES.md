# 冷光线

![Node on the shelf](images/EL Wire Driver.png)

让冷光线在黑暗中发出微光。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-elwires.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：节能灯。

- 说明：当夜幕降临的时候，房子屋檐的灯就会亮起，为回家的人照明家的方向。当天色变亮的时候，灯就会自动关闭，节能省电。

- 所需模块：电源、蓝牙模块、光线传感器、冷光管驱动、冷光管、连接线10cm。