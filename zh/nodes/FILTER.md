# 过滤 

![FILTER Node on the shelf](images/node-filter-shelf.png)
 
过滤掉不在范围内的数值。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/filter.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小台灯

- 功能：15：00：00时灯自动亮起，15：30：00时自动熄灭

### 工作原理

![FILTER Node on the canvas](images/node-filter-canvas.png)
 
在配置项中设定一组范围。如果输入落在范围内，则输出输入本身；否则，输出no。 
