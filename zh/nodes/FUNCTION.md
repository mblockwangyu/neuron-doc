# Function

![FUNCTION Node on the shelf](images/node-function-shelf.png)
 
对数值进行函数运算

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/function.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数字标签

- 功能：显示数字的倍数


### 工作原理
 
![FUNCTION Node on the canvas](images/node-function-canvas.png)
 
在配置面板中设置要使用的函数。输出将始终是输入的函数计算值。

函数的设置面板

![FUNCTION Node example](images/node-function-example-1.png)
 
示例:一个可以将整数从1设置为10的旋钮
