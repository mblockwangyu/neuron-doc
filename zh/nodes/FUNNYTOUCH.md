# 触摸开关(四控)

![Node on the shelf](images/Funny touch.png)

判断地线和四色鳄鱼夹是否导通

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-funnytouch-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：尤克里里

- 说明：触摸不同的鳄鱼夹，会发出不同的声音

- 所需模块：智能电源 × 1、LED面板 × 1、蜂鸣器 × 1、触摸开关（四控）× 1、四色鳄鱼夹 × 1、地线 × 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-funnytouch-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
