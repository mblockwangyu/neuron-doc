# 保持
 
![HOLD Node on the shelf](images/node-hold-shelf.png)

让事件持续发生一段时间

### 用法

“保持”节点可以： 

-	保持输入的值，直到另一个“no”没有值出现 

-	在一段时间内保持输入的值不变 

-	让输入逐渐的变化 

<video width="512" height="384" controls>
  <source src="../../node-videos/hold.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小台灯

- 功能：按下按钮后松开手，灯亮起5s后才会灭

### 工作原理
 
![HOLD Node on the canvas](images/node-hold-canvas.png)

你可以从配置项的3种模式中任选1个：

![Node config](images/node-hold-config-1.png)
 
保持直到改变：输入将会被保持，直到有另一个输入进来（例如一张伤心的脸取代了一张微笑的脸）。换句话说，输出将会是除了“no”之外的任何值。  
 
![Node example](images/node-hold-example-1.png)

例如：用多触摸传感器改变RGB LED的颜色（但不要在未触摸时关掉灯） 

持续时间：输入会在一段时间内保持不变。在此期间，任何输入的值将被忽略。
如果已经超过了保持时长当输入依然为“no”，那么输出将会被设置为“no”。 

![Node example](images/node-hold-example-2.png)

检测到光后发出3秒哔哔声的报警装置。 

缓慢变化：输出将根据输入而改变，但如果输入的是一个数值，每秒钟的变化将不会超过指定数字。 

![Node example](images/node-hold-example-3.png)

当按下按钮时，光线会慢慢从绿色变为红色(保持节点配置的数量设置为3)。 