# 温湿度传感器

![Node on the shelf](images/Humiture Sensor.png)

检测空气温湿度。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-humiture.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：自动收衣架

- 说明：用舵机模块来控制双舵机转动的角度，温湿度传感器能感知环境的湿度。使用神经元APP可以调试舵机的角度，让双舵机转动的角度一致，设置湿度的数值，当湿度大于该数值就收进衣服，低于该数值则将衣服晒出去。

- 所需模块：电源、蓝牙模块、温湿度传感器、双舵机驱动、舵机组件包、连接线20cm、磁吸板、插销。