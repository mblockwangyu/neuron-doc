# 指示灯

![Node on the shelf](images/Indicator.png)

这是一颗指示灯。有“亮”和“灭”两种状态。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/toggle.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小台灯

- 功能：按下按钮灯亮，松开灯灭