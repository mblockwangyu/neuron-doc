# 重复
 
![INTERVAL Node on the shelf](images/node-interval-shelf.png)

让事件反复发生

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/repeat.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：坏掉的小台灯

- 功能：不停闪烁

### 工作原理
 
![INTERVAL Node on the canvas](images/node-interval-canvas.png)

在配置项中可以选择每隔多少秒反转一次 

![Node example](images/node-interval-example-1.png)

例：制作一个闪烁的灯 

![Node example](images/node-interval-example-2.png)

例：温度过高时会发出“哔哔”声报警 