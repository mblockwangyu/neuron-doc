# 摇杆
 
![INTERVAL Node on the shelf](images/Joystick.png)

输出摇杆的上下(-100~100)、左右(-100~100)的位置。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-joystick.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：摇杆小车

- 说明：电机模块来驱动小车驱动，摇杆模块来触发电机模块，无线接收模块配对小车与摇杆，前后左右摇动摇杆，让小车行走。

- 所需模块：电源、无线接收、摇杆、双直流电机驱动、直流电机套件包、磁吸板、插销。
