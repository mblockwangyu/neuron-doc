# 标签
 
![INTERVAL Node on the shelf](images/Label.png)

显示字母、数字、yes和no

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/random-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：数字标签

- 功能：显示数字改变的规律