# LED灯
 
![INTERVAL Node on the shelf](images/REG LED.png)

发出指定颜色的光

### 用法

![NUMBER Node on the canvas](images/ex-RGB-LED.jpg)

- 案例：紧急搜救器

- 说明：无人状态下LED灯珠长亮绿灯，当检测到有人时LED灯珠长亮红灯并且蜂鸣器发出警报声

- 所需模块：智能电源、人体红外传感器、LED灯珠、蜂鸣器"