# LED面板
 
![INTERVAL Node on the shelf](images/LED Panel.png)

在8x8的LED点阵上绘画或显示动画

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：无限手环

- 说明：声音传感器可以检测到声音大小，让LED面板根据信号给到反馈

- 所需模块：智能电源 × 1、LED面板 × 1、测距传感器 × 1、声音传感器 × 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
