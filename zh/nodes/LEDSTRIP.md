# LED灯带
 
![INTERVAL Node on the shelf](images/LED Strip.png)

让一串LED灯珠发出各种颜色的光

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


-案例：云朵灯玩法1

-说明：靠近就会变色。测距传感器检测距离，LED灯带控制颜色变化

-所需模块：智能电源 × 1、灯带 × 1、测距传感器 × 1、灯带驱动 × 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>