# 双路红外开关

![INTERVAL Node on the shelf](images/Line Follower.png)

判断传感器是否在深色表面上

### 用法

- 案例：小台灯

- 说明：靠近左边的传感器，灯会变成红色；靠近右边的传感器，灯会变成绿色

- 所需模块：智能电源、双路红外开关