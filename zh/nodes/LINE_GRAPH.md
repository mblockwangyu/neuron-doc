# 折线图
 
![INTERVAL Node on the shelf](images/Line-Graph.png)

实时显示传感器数据。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/graph-average.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数据变化趋势

- 功能：显示数字改变的规律