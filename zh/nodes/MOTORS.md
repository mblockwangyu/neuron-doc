# 双电机驱动
 
![INTERVAL Node on the shelf](images/DC Motor Driver.png)

根据指定速度转动电机

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-motor-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：探索者号

- 说明：双电机驱动带动两个电机转动，从而让小车跑起来。

- 所需模块：智能电源 × 1、LED面板 × 1、蜂鸣器 × 1、双直流电机驱动 × 1、灯带 × 1、测距传感器 × 1、直流电机 × 2、灯带驱动 × 1

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-motor-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>