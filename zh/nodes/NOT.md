# 否则

![Node on the shelf](images/node-not-shelf.png)

如果输入是Yes，输出No。否则输出Yes

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/not.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

-案例：小台灯

-功能：按下按钮关灯，松开常亮

### 工作原理

![Node on the shelf](images/node-not-canvas.png)

只需要连接输入，就会的到相反的输出 

![Node example](images/node-not-example-1.png)

例如：如果按你被按下，变红；否则，变绿 

![Node example](images/node-not-example-2.png)

在红色和绿色之间循环 