# 现在时间 

![NOW Node on the shelf](images/node-now-shelf.png)
 
获取现在的时间（时、分、秒）

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/or-compare.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小闹钟

- 功能：今天15：00：00后，响起猫叫声小闹钟


### 工作原理
 
![NOW Node on the canvas](images/node-now-canvas.png)

这个节点中有3个输出，含义很简单:当前时间的时(H)、分(M)和秒(S)。

![NOW Node example](images/node-now-example-1.png)
 
例：一个在7点10分会警报的蜂鸣器闹钟 