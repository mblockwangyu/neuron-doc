# 数字
 
![NUMBER Node on the shelf](images/node-number-shelf.png)

输出一个固定数值，可以用于电机速度、舵机角度、灯光亮度等。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/number.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数字标签

- 功能：向标签赋值

### 工作原理
 
![NUMBER Node on the canvas](images/node-number-canvas.png)

配置项中的数值将会被传送到输出。 
如果提供了输入，那么只有在输入为“yes”时才会发送该值；否则将会输出“no”。 

![NUMBER Node example](images/node-number-example-1.png)

例如：将电机转速设置为50 

![NUMBER Node example](images/node-number-example-2.png)
 
例如：当光线传感器的值大于50的时候，将电机转速设置为70 
