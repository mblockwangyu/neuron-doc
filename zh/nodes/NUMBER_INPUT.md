# 数字输入
 
![NUMBER Node on the shelf](images/node-number-shelf.png)

可以输入数值。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/numbercontrol.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：数字标签

- 功能：显示输入的数值
