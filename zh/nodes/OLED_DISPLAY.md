# 显示屏
 
![NUMBER Node on the shelf](images/Display.png)

显示文字或者表情。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-button-knob-display.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：保险箱

- 说明：使用旋钮制作密码器，通过显示屏显示密码。输入密码正确后指示灯亮起，按下按钮保险箱被开启。

- 所需模块：电源、蓝牙模块、按钮、旋钮、显示屏、双舵机驱动、舵机驱动包、转接线10cm。
