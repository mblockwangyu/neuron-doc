# 或

![OR Node on the shelf](images/node-or-shelf.png)
 
连接的多个输入有一个为Yes则输出Yes。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/or-compare.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小闹钟

- 功能：逢15就猫叫

### 工作原理

![OR Node on the canvas](images/node-or-canvas.png)
 
当输入为“yes”时，“或”节点为“yes”。 

![Node example](images/node-or-example-1.png)
 
例如：一种报警装置，当被暴露在光强下（光强大于30），或者被移动（陀螺仪被震动）时报警。 

请注意上面的程序等于： 

![Node example](images/node-or-example-1.png)

提示 

-	事实上，你不需要经常使用“或”节点，因为“或”逻辑被放置到大多数节点的输入中。