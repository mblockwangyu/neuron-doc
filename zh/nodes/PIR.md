# 人体红外传感器

![OR Node on the shelf](images/PIR Sensor.png)
 
检测是否有活物靠近。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-PIR.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：紧急搜救器

- 说明：无人状态下LED灯珠长亮绿灯，当检测到有人时LED灯珠长亮红灯并且蜂鸣器发出警报声

- 所需模块：智能电源、人体红外传感器、LED灯珠、蜂鸣器"
