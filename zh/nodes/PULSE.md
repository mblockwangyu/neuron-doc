# Pulse

![PULSE Node on the shelf](images/node-pulse-shelf.png)
 
Make the output cycles over time.

### 用法

The PULSE node’s output changes with the time according to the waveform specified in the config panel. It can be used to:

-	Make a “breathing” light/sound/movement effect; make the movement looks smooth

### 工作原理
 
![PULSE Node on the canvas](images/node-pulse-canvas.png)

In the settings panel, you can set the following parameters of the pulse generated:
 
-	Waveform: it can be “sin”, “square” and “triangle”. “Sin” waveform looks like breathing: it slows down at the top and bottom value. “Square” acts similar to the “INTERVAL” note, it turns on and off at each time period. “Triangle” is steadier(linear) when changing, but drives in the opposite way sharply when it reaches its maximum and minimum value.
-	Wavelength: the time in seconds that a pulse reaches its full cycle and starts to repeat itself.
-	Amplitude: the maximum value and (the negative) minimum value of the pulse 

![PULSE Node example](images/node-pulse-example-1.png)

Example: a breathing light

提示 

-	If you want a pulse moves around some point other than zero, use the “COMPUTE” node.
