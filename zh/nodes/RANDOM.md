# 随机数 

![RANDOM Node on the shelf](images/node-random-shelf.png)
 
每次产生一个随机数。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/random-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：掷骰子

- 功能：设置随机数范围1-6，每按一下按钮，产生一个随机数

### 工作原理
 
当输入由no变为yes时，一个随机数将会被传送到输出。你可以通过配置改变随机数的范围

![RANDOM Node example](images/node-random-example-1.png)

例如：一个随机变色的灯  
