# 测距传感器

![RANDOM Node on the shelf](images/ranging sensor.png)
 
检测距离远近。范围在2-200之间。

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-ranging-LEDstrip-1.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：无限手环

- 说明：测距传感器可以检测到物体是否靠近，让LED面板根据信号给到反馈

- 模块清单：智能电源 × 1、LED面板 × 1、测距传感器 × 1、声音传感器 × 1

