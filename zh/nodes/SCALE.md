# 映射 
 
![SCALE Node on the shelf](images/node-scale-shelf.png)

将一个范围的数字映射到另一个范围

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/scale.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数据变化趋势

- 功能：加入映射和不加映射的折线图形成对比，显示数字改变的规律

### 工作原理

![SCALE Node on the canvas](images/node-scale-canvas.png)
 
从配置项中设定两个范围。输入将会按照指定的范围映射。 

![SCALE Node example](images/node-scale-example-1.png)
 
Example: make a thermometer. The temperature (0~50) is scaled to the angle of the servo (0~70) that has a needle stick on it that acts as a pointer.例如：温度计。温度(0~50)被映射到舵机(0~70)的角度，在它上面有一个指针作为指示。 
提示

-	你可以用映射节点将取值范围反转，请看下面的例子： 

![SCALE Node example](images/node-scale-example-2.png)

例如：做一个随温度变化的变色灯。当天热时，它会变红，天冷时它会变蓝。第一个映射节
点将温度的变化范围映射为灯的取值范围0-255（RGB值的取色范围）。第二个映射节点
将“蓝色”的RGB值对调变为红色。 
