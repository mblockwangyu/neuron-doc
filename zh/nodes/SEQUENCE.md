# 间隔

![SEQUENCE Node on the shelf](images/node-sequence-shelf.png)
 
间隔一段时间后，发生下一件事情

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/sequence.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：小闹钟

- 功能：点击按钮发出猫叫声，间隔5秒后发出狗叫声

### 工作原理
  

将动作连接到间隔节点的输入上；连接到每一个输出的动作；点击节点上的数字来设置每个
输出的持续时间。并使用加号和减号来增加/删除输出。 

![SEQUENCE Node example](images/node-sequence-example-1.png)

例如：一个红绿灯绿灯亮10秒，黄灯亮3秒，红灯亮30秒。 

![SEQUENCE Node example](images/node-sequence-example-2.png)
 
例如：当设备前检测到有人（由人体红外传感器检测到）时，通过反复晃动端口1的舵机来
摇头；否则，通过反复晃动端口2的舵机来摇头。 
