# 舵机

![SEQUENCE Node on the shelf](images/Servo Driver.png)
 
让舵机摆在指定角度上。

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-servo.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：拔萝卜

- 说明：当萝卜没有被拨出来的时候，是静止不动的，但是，当萝卜被拔出时，会伸出两个”不”字左右晃动，仿佛在说“ 不要拔我，不要拔我”。

- 所需模块：电源、陀螺仪、双舵机驱动、舵机组件包"

