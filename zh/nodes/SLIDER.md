#滑块

![SEQUENCE Node on the shelf](images/Slider.png)
 
这是一个滑杆。拖动滑杆，改变数值的大小。

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/slider-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：数字标签

- 功能：显示数字改变的规律

