# 土壤湿度传感器

![SEQUENCE Node on the shelf](images/node-sequence-shelf.png)
 
检测土壤湿度

### 用法

- 案例：懒人浇花器

- 说明：通过土壤传感器检测土壤干湿度，如果土壤湿度过低则显示哭脸需要浇水；如果土壤湿度过高则显示笑脸表示无需浇水。

- 所需模块：智能电源、土壤传感器、LED面板。

