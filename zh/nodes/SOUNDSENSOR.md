# 声音传感器

![SEQUENCE Node on the shelf](images/Sound Sensor.png)
 
检测声音大小。范围在1-100之间。

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-sound-LEDpanel-2.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：无限手环

- 说明：声音传感器可以检测到声音大小，让LED面板根据信号给到反馈。

- 模块清单：智能电源 × 1、LED面板 × 1、测距传感器 × 1、声音传感器 × 1。

