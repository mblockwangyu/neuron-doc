# 温度传感器

![SEQUENCE Node on the shelf](images/Temp Sensor.png)
 
检测物体的温度。

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/ex-temp.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：温度计

- 说明：通过将温度传感器检测到的温度值转化为舵机转动的角度值，来展示度数。

- 所需模块：智能电源、温度传感器、双舵机驱动、舵机。

