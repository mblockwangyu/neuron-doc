# 文本输入

![SEQUENCE Node on the shelf](images/Text-Input.png)
 
可以输出文本。

### 用法

![SEQUENCE Node on the canvas](images/node-sequence-canvas.png)

<video width="512" height="384" controls>
  <source src="../../node-videos/textbox-label.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：标签标签

- 功能：显示输入的字母。

