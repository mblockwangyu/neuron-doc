# 今日日期 

![TODAY Node on the shelf](images/node-today-shelf.png)
 
获取今天的日期（年、月、日）

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/devicespeaker.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>


- 案例：小闹钟

- 功能：2019年12月25日，播放圣诞快乐歌