# 阀门

![VALVE Node on the shelf](images/node-valve-shelf.png)

当上面的”阀门“打开时，输出下面的数值

### 用法

<video width="512" height="384" controls>
  <source src="../../node-videos/valve.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>

- 案例：数字标签

- 功能：显示通过的数字。

### 工作原理
 
上面的输入接收为YES/NO;下面的输入接收为数值。如果上面的输入是YES，那么下面的输入将被发送到节点的输出;否则，节点将输出NO。
