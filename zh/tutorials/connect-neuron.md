# 连接神经元

连接神经元到慧编程，需要使用到：Micro-USB 数据线、蓝牙或者 Wi-Fi模块。

### 使用蓝牙模块连接

1\. 使用 Micro-USB 数据线将蓝牙模块连接到电脑的 USB 口，如下图所示：

<img src="../../en/tutorials/images/usb-mblock.png" width="400px;" style="paddings:3px 3px 12px 0px;">

2\. 在“设备”下，点击“+”，从设备库中添加神经元，然后点击“连接”。

<img src="images/connect.gif" width="800px;" style="paddings:3px 3px 12px 0px;">