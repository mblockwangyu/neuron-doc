# 快速上手

我们将通过程序点亮神经元的LED灯带，需要使用<a href="../hardware/output/led-strip-driver.html" target="_blank">「灯带驱动」</a>和<a href="../hardware/output/led-strip.html" target="_blank">「灯带」</a>两个模块。

1\. 连接「灯带驱动」和「灯带」

2\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当绿色旗帜被点击</span> 拖到脚本区

<img src="images/get-started-1.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 添加外观类积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">灯带（1）亮起（）</span>

<img src="images/get-started-2.gif" style="padding:3px 0px 15px 0px; width:800px;">

3\. 点击绿色旗帜，看灯带是不是亮起了！

<img src="images/get-started-3.png" style="padding:3px 0px 15px 0px;">