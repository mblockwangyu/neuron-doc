<img src="images/neuron.jpg" style="width:600px;padding:3px 3px 16px 0px;">

# 简介

神经元平台包含声音传感器、光线传感器、人体红外传感器等30多个可编程电子模块，可以实现上百种互动，不仅能够满足孩子在听觉、视觉、体感等多个方面的创造需求，还能吸引孩子的兴趣，让孩子在玩耍中了解科学原理与现象，提升创造能力。

### 模块分类

神经元平台的电子模块以颜色进行分类，有以下几个模块：

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 1 &nbsp;</span> **能源&通信模块**

为其他模块供电、提供多种无线通信方式。

<img src="images/CommunicationModule.png" style="width:400px;padding:3px 3px 16px 0px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 2 &nbsp;</span> **输入模块**

从外部环境中收集信息（如声音和光），并发送信号至输出模块，从而改变输出。

<img src="images/InputModule.png" style="width:400px;padding:3px 3px 16px 0px;">

<span style="color:white;font-size:18;border-radius:100%;background-color:lightblue;font-weight:bold;">&nbsp; 3 &nbsp;</span> **输出模块**

接收来自输入模块的信号，并根据信号作出相应的反应，如亮灯、移动或发声。

<img src="images/OutputModule.png" style="width:400px;padding:3px 3px 16px 0px;">

---

### 慧编程

结合慧编程软件，你可以通过编程实现更多创意。你可以下载慧编程 PC端，或者访问网页端，开始你的编程之旅。

<img src="images/mblock5.png" style="width:200px;padding:3px 3px 16px 0px;">

- PC 端：访问 [http://www.mblock.cc/mblock-software/](http://www.mblock.cc/mblock-software/) 下载软件
- 网页端：[https://ide.makeblock.com/](https://ide.makeblock.com/)

<small>注：网页端需配合 mLink 插件，请参考 <a href="http://www.mblock.cc/doc/zh/part-one-basics/mlink-quick-start-guide.html" target="_blank">mLink 指南</a>。</small>

---

### 神经元 APP

神经元App 是神经元电子积木的配套软件，连线即编程的设计，让孩子无需学习复杂的代码，只需连线组合电子积木与相应的软件节点，就能实现各种好玩有趣的应用。

<img src="images/neuron.png" style="width:200px;padding:3px 3px 16px 0px;"> &nbsp; &nbsp; &nbsp; &nbsp;<img src="images/neuron-QR-code.png" style="width:100px;padding:3px 3px 16px 0px;">

- 下载方式：可在各应用平台搜索“神经元”，或扫描二维码。

---

<div style="background-color:#FFFFCC;padding:10px;border-left:5px solid #FFEB3B;border-radius:5px;line-height:28px;">
<strong>注：</strong><br>
<p>此帮助文档主要介绍结合慧编程使用神经元，关于神经元APP的使用，可参考章节 <a href="../neuron-app.html">「神经元 APP」</a>。</p>
</div>







