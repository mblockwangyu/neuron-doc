<img src="images/with-codey.jpg" style="padding:3px 0px 15px 0px;width:600px">

# 结合程小奔编程

神经元可以连接到小程，进行编程。下面我们来做一个简单的项目：通过小程的A、B按钮来切换神经元LED面板的颜色。

1\. 将神经元 <a href="../hardware/output/led-panel.html" target="_blank">「LED面板」</a> 模块和小程连接。

2\. 连接小程到慧编程，选择“设备”下的“程小奔”，并连接。

<img src="images/with-codey-0.png" style="padding:3px 0px 15px 0px;">

3\. 点击积木区最下方的“+ 添加扩展”，添加“神经元”扩展。

<img src="images/with-codey-1.gif" style="padding:3px 0px 15px 0px;width:800px;">

4\. 将事件类积木 <span style="background-color:#FFBF00;color:white;padding:3px; font-size: 12px">当按下按钮（A）</span> 拖到脚本区，再添加
神经元积木 <span style="background-color:#9966FF;color:white;padding:3px; font-size: 12px">LED面板（1）点亮：x（0）y（0），颜色为（红色）</span>

<img src="images/with-codey-2.gif" style="padding:3px 0px 15px 0px;width:800px;">

5\. 复制程序，并选择按钮“A”和颜色“绿色”

<img src="images/with-codey-3.gif" style="padding:3px 0px 15px 0px;width:800px;">

6\. 上传程序

<img src="images/with-codey-4.gif" style="padding:3px 0px 15px 0px;width:800px;">

7\. 按下小程的按钮看看吧！