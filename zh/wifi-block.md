# 使用 Wi-Fi 模块

* [扬声器和麦克风](wifi-mic-speaker.md)
  * [播放音符](nodes/PLAYNOTES.md)
  * [播放声音](nodes/PLAYSOUND.md)
  * [录制](nodes/RECORD.md)
  * [文字转语音](nodes/TEXTTOSPEECH.md)
* [摄像头](wifi-camera.md)
  * [表情测试](nodes/EMOTION_TEST.md)
  * [图片转文字](nodes/OCR.md)
  * [相框](nodes/PHOTO_FRAME.md)
  * [拍摄图片](nodes/SNAPSHOT.md)